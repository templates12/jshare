package com.example.guiclient.model;

import java.time.LocalDateTime;

import com.example.guiclient.Helpers;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Component
public class TTMTableForm {

    private Long id;
    private String ttmReference;
    private String type;
    private int style;
    private LocalDateTime createdAt;
    private String senderBic;
    private String businessReference;
    private String receiverBic;
    private String status;

    public TTMTableForm( Map<String, Object> ttmMap ) {
        if (ttmMap.get("id") != null)
            this.id = (Long) ttmMap.get("id");
        if (ttmMap.get("ttmReference") != null)
            this.ttmReference = (String) ttmMap.get("ttmReference");
        if (ttmMap.get("type") != null)
            this.type = (String) ttmMap.get("type");
        if (ttmMap.get("style") != null)
            this.style = (int) ttmMap.get("style");
        if (ttmMap.get("createdAt") != null)
            this.createdAt = LocalDateTime.parse((CharSequence)ttmMap.get("createdAt"));
        if (ttmMap.get("senderBic") != null)
            this.senderBic = (String) ttmMap.get("senderBic");
        if (ttmMap.get("businessReference") != null)
            this.businessReference = (String) ttmMap.get("businessReference");
        if (ttmMap.get("receiverBic") != null)
            this.receiverBic = (String) ttmMap.get("receiverBic");
        if (ttmMap.get("status") != null)
            this.type = (String) ttmMap.get("type");
    }

    @Override
    public String toString() {
        Map<String, Object> req = new HashMap<>();
        req.put("ttmReference", ttmReference);
        req.put("type", type);
        req.put("style",style);
        req.put("createdAt", createdAt);
        req.put("senderBic", senderBic);
        req.put("businessReference", businessReference);
        req.put("receiverBic", receiverBic);
        req.put("status", status);
        return Helpers.map2json(req);
    }
}
