package com.example.guiclient.model;

import com.example.guiclient.Helpers;
import lombok.*;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Component
public class TTMTableUpdateForm {

    private String status;
    private String ttmReference;
    private String branchLocation;
    private String newBranchLocation;
    private String businessReference;
    private String newBusinessReference;
    private String systemId;
    private String newSystemId;
    private int customerId;
    private int newCustomerId;
    private String otherSystem;
    private String newOtherSystem;
    private String swiftContent;

    public TTMTableUpdateForm( Map<String, Object> ttmMap ) {
        if (ttmMap.get("ttmReference") != null)
            this.ttmReference = (String) ttmMap.get("ttmReference");
        if (ttmMap.get("branchLocation") != null)
            this.branchLocation = (String) ttmMap.get("branchLocation");
        if (ttmMap.get("businessReference") != null)
            this.businessReference = (String) ttmMap.get("businessReference");
        if (ttmMap.get("customerId") != null)
            this.customerId = (int) ttmMap.get("customerId");
        if (ttmMap.get("systemId") != null)
            this.systemId = (String) ttmMap.get("systemId");
        if (ttmMap.get("otherSystem") != null)
            this.otherSystem = (String) ttmMap.get("otherSystem");
        if (ttmMap.get("status") != null)
            this.status = (String) ttmMap.get("status");
        //if (ttmMap.get("swift") != null)
        this.swiftContent = "SWIFT content to be implemented in TTMTable";
    }

    public void updateNewValues(TTMTableUpdateForm newUpdateForm) {
        if (!newUpdateForm.getNewBranchLocation().isEmpty()) {
            this.branchLocation = newUpdateForm.getNewBranchLocation();
        }
        if (!newUpdateForm.getNewBusinessReference().isEmpty()) {
            this.businessReference = newUpdateForm.getNewBusinessReference();
        }
        if (!newUpdateForm.getNewSystemId().isEmpty()) {
            this.systemId = newUpdateForm.getNewSystemId();
        }
        if (newUpdateForm.getNewCustomerId() > -1) {
            this.customerId = newUpdateForm.getNewCustomerId();
        }
        if (!newUpdateForm.getNewOtherSystem().isEmpty()) {
            this.otherSystem = newUpdateForm.getNewOtherSystem();
        }
    }

    @Override
    public String toString() {
        Map<String, Object> req = new HashMap<>();
        req.put("status", status);
        req.put("ttmReference", ttmReference);
        req.put("branchLocation", branchLocation);
        req.put("businessReference",businessReference);
        req.put("systemId", systemId);
        req.put("customerId", customerId);
        req.put("otherSystem", otherSystem);
        return Helpers.map2json(req);
    }
}
