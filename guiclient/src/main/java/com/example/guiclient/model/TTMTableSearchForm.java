package com.example.guiclient.model;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import com.example.guiclient.Helpers;
import lombok.*;
import org.springframework.stereotype.Component;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Component
public class TTMTableSearchForm {
    private String createdAt;
    private String ttmRef;
    private String status;
    private String businessReference;
    private String senderBic;
    private String tag20;

    /**
     * checks search criteria for table
     *
     * @return boolean true if only one is selected otherwise false
     */
    public boolean checkCriteria() {
        int ncount = 0;
        Field fields[] = this.getClass().getDeclaredFields();
        for (Field f : fields) {
            try {
                Object value = f.get(this);
                if (value != null && !(value.toString()).isEmpty()) {
                    ncount++;
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        if (ncount > 1 || ncount == 0)
            return false;
        return true;
    }

    /**
     * checks DATE search criteria for table @param None @return boolean true if
     * only one is selected otherwise false @throws
     */
    public boolean checkDate() {
        if (createdAt != null && !createdAt.isEmpty()) {
            LocalDateTime currentDate = LocalDateTime.now();
            LocalDate currentLocalDate = currentDate.toLocalDate();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate enteredDate = LocalDate.parse(createdAt, formatter);

            Period period = Period.between(currentLocalDate, enteredDate);
            int compareValue = period.getDays();

            if (compareValue > 30) {
                return false;
            }
            if (compareValue < 0) {
                return false;
            }
        }
        return true;
    }

    public boolean checkError() {
        return checkDate() && checkCriteria();
    }

    @Override
    public String toString() {
        Map<String, Object> req = new HashMap<>();
        if (status != null && !status.isEmpty()) {
            req.put("status", status);
        }
        if (ttmRef != null && !ttmRef.isEmpty()) {
            req.put("ttmReference", ttmRef );
        }
        if (businessReference != null && !businessReference.isEmpty()) {
            req.put("businessReference", businessReference);
        }
        if (senderBic != null && !senderBic.isEmpty()) {
            req.put("senderBic", senderBic);
        }
        if (tag20 != null && !tag20.isEmpty()) {
            req.put("tag20", tag20);
        }
        if (createdAt != null && !createdAt.isEmpty()) {
            req.put("createdAt", createdAt);
        }
        return Helpers.map2json(req);
    }
}