package com.example.guiclient;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@Service
public class RESTClientService {

    private static final Logger logger = Logger.getLogger(RESTClientService.class.getName());
    private RestTemplate rest;
    private String httpsURL;

    String host = "localhost";
    int port = 9001;

    /**
     * constructor
     **/
    public RESTClientService(RestTemplate rest) {
        this.rest = rest;
        this.httpsURL = "http://" + host + ":" + port;
        logger.info(this.httpsURL);
    }

    /**
     * Get all filtered records from Table
     *
     * @param filterRequestJSON String json filter
     **/
    public String getdDataByReference(final String filterRequestJSON) {
        if (filterRequestJSON != null && !filterRequestJSON.isEmpty()) {
            final String url = this.httpsURL + "/guiservice/get";
            try {
                logger.info(String.format("Searching by reference number: %s", filterRequestJSON));
                HttpHeaders httpHeaders = new HttpHeaders();
                httpHeaders.set("Content-Type", "application/json");
                Map<String, Object> req = new HashMap<String, Object>();
                req.put("ttmReference", "filterRequestJSON");
                String searchByReference = Helpers.map2json(req);
                HttpEntity<String> httpEntity = new HttpEntity<>(searchByReference, httpHeaders);
                String response = rest.postForObject(url, httpEntity, String.class);
                if (response != null && !response.isEmpty()) {
                    return response;
                }
            } catch (RestClientException e) {
                e.printStackTrace();
            }
        }
        return new String();
    }

    /**
     * Get all filtered records from Table
     *
     * @param filterRequestJSON String json filter
     **/
    public String getFileredData(final String filterRequestJSON) {
        if (filterRequestJSON != null && !filterRequestJSON.isEmpty()) {
            final String url = this.httpsURL + "/guiservice/filter";
            try {
                logger.info(String.format("Filtering data by: %s", filterRequestJSON));
                HttpHeaders httpHeaders = new HttpHeaders();
                httpHeaders.set("Content-Type", "application/json");
                HttpEntity<String> httpEntity = new HttpEntity<>(filterRequestJSON, httpHeaders);
                String response = rest.postForObject(url, httpEntity, String.class);
                if (response != null && !response.isEmpty()) {
                    return response;
                }
                //ResponseEntity<TTMTableResult> response = rest.exchange(url, HttpMethod.POST, httpEntity,
                //        new ParameterizedTypeReference<TTMTableResult>() {
                //        });
                //if (response.hasBody()) {
                //    return response.getBody();
               // }
            } catch (RestClientException e) {
                e.printStackTrace();
            }
        }
        return new String();
    }

    /**
     * Update record from Table
     *
     * @param filterRequestJSON String json filter
     **/
    public String updateTableData(final String filterRequestJSON) {
        if (filterRequestJSON != null && !filterRequestJSON.isEmpty()) {
            logger.info(String.format("Updating data: %s", filterRequestJSON));
            final String url = this.httpsURL + "/guiservice/update";
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.set("Content-Type", "application/json");
            HttpEntity<String> httpEntity = new HttpEntity<>(filterRequestJSON, httpHeaders);
            String response = rest.postForObject(url, httpEntity, String.class);
            if (response != null && !response.isEmpty()) {
                return response;
            }
        }
        return new String();
    }
}