package com.example.guiclient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.guiclient.model.TTMTableForm;
import com.example.guiclient.model.TTMTableSearchForm;
import com.example.guiclient.model.TTMTableUpdateForm;

@Controller
@RequestMapping("/guiclient")
public class AppController {

    private static final Logger LOG = Logger.getLogger(AppController.class.getName());

    @Autowired
    private RESTClientService ttmService;
    @Autowired
    private TTMTableSearchForm searchForm;
    @Autowired
    private TTMTableUpdateForm updateForm;
    @Autowired
    private TTMTableForm tableForm;

    public AppController(RESTClientService ttmService, TTMTableSearchForm searchForm, TTMTableUpdateForm updateForm, TTMTableForm tableForm) {
        this.ttmService = ttmService;
        this.searchForm = searchForm;
        this.updateForm = updateForm;
        this.tableForm = tableForm;
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/403")
    public String error403() {
        return "error403";
    }

    @GetMapping("/home")
    public String homeFormGet(Model model) {
        setPageData(model);
        return "index";
    }

    @PostMapping("/search")
    public String searchFormPost(@ModelAttribute("searchForm") TTMTableSearchForm searchForm, BindingResult result) {
        if (!result.hasErrors()) {
            setSearchForm(searchForm);
        }
        return "redirect:/guiclient/home";
    }

    @GetMapping("/update/{ttmReference}")
    public String doGetUpdate(@PathVariable(name = "ttmReference") String ttmReference, Model model) {
        TTMTableUpdateForm updateForm = null;
        String record = null;
        try {
            record = ttmService.getdDataByReference(ttmReference);
            if (record != null) {
                Map<String, Object> ttmMap = Helpers.json2map(record);
                updateForm = new TTMTableUpdateForm(ttmMap);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        setUpdateForm(updateForm);
        return "redirect:/guiclient/home";
    }

    @PostMapping("/update")
    public String doPostUpdate(@ModelAttribute("updateForm") TTMTableUpdateForm updateForm, BindingResult result, Model model) {
        getUpdateForm().updateNewValues(updateForm);
        try {
            if (ttmService.updateTableData(getUpdateForm().toString()) != null) {
                setPageData(model);
                setUpdateForm(new TTMTableUpdateForm());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/guiclient/home";
    }

    private void setPageData(Model model) {
        model.addAttribute("searchForm", getSearchForm());
        model.addAttribute("updateForm", getUpdateForm());
        model.addAttribute("searchError", !getSearchForm().checkError());
        try {
            String values = ttmService.getFileredData(getSearchForm().toString());
            List<TTMTableForm> records = new ArrayList<>();
            if (values != null && !values.isEmpty()) {
                JSONTokener parser = new JSONTokener(values);
                JSONArray obj = new JSONArray(parser);
                for (Object record : obj) {
                    JSONObject tmp = (JSONObject) record;
                    TTMTableForm recForm = new TTMTableForm(tmp.toMap());
                    records.add(recForm);
                }
            }
            model.addAttribute("allRecords", records);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("statusList", getStatusList());
        model.addAttribute("swiftContent", getUpdateForm().getSwiftContent());
    }

    public List<String> getStatusList() {
        return Arrays.asList("unrouted", "dispatched", "processed");
    }

    public TTMTableSearchForm getSearchForm() {
        return searchForm;
    }

    public void setSearchForm(TTMTableSearchForm searchForm) {
        this.searchForm = searchForm;
    }

    public TTMTableUpdateForm getUpdateForm() {
        return updateForm;
    }

    public void setUpdateForm(TTMTableUpdateForm updateForm) {
        this.updateForm = updateForm;
    }
}
