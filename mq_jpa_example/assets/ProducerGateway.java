package demo2;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

@MessagingGateway
@EnableBinding(ProducerChannels.class)
interface ProducerGateway {

	@Gateway(requestChannel = ProducerChannels.DIRECT)
	void directPut(String msg);

	@Gateway(requestChannel = ProducerChannels.BROADCAST)
	void broadcastPut(String msg);
}
