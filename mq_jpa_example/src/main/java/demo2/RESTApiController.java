package demo2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;
import java.util.Optional;
import java.util.HashMap;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/ttmtableapi")
public class RESTApiController {
	private final Logger log = LoggerFactory.getLogger(getClass());
	private JPATTMTableRepository ttmtableRepository;    

	@Autowired
    public RESTApiController(JPATTMTableRepository tRepository) {
        this.ttmtableRepository = tRepository;
    }

    /**
     * get ver
     * @param  None
     * @return json
     * @throws None
     */
    @PostMapping("/ver")
    @ResponseBody
    public Map<String, Object> getVer() {
        Map<String, Object> reply = new HashMap<String, Object>();
        reply.put("version", "1.0");
        log.debug("getVer:" + reply.toString());
        return reply;
    }

    /**
     * MQ test
     * @param  None
     * @return json
     * @throws None
     */
    @PostMapping("/mqtest")
    @ResponseBody
    public ResponseEntity<?> sendMQ() {
        return ResponseEntity.ok("OK");
    }

    /**
     * create one records in DB
     * @param  ttmJson - json string
     * @return TTMTable obj as json
     * @throws None
     */
    @PostMapping("/new")
    public java.util.Optional<JPATTMTable> setTable() {
		JPATTMTable ttm = new JPATTMTable();
		ttm.ttmReference = Helpers.generateString(12);
		ttmtableRepository.save(ttm);
        ttm.setCreateAt(LocalDateTime.now());
        ttmtableRepository.save(ttm);
        return ttmtableRepository.findById(ttm.getId());
    }

    /**
     * get all records from DB
     * @param  None
     * @return List
     * @throws None
     */
    @PostMapping("/all")
    public Iterable<JPATTMTable> getAll() {
        return ttmtableRepository.findAll();
    }

    /**
     * get all records from DB by filer
     * @param  json filter
     * @return List
     * @throws None
     */
    @PostMapping("/filter")
    public Iterable<JPATTMTable> getByFilter(@RequestBody String ttmJson) {
        log.info("\n.............. filter::jsondata ............ ="+ttmJson.toString());
        Map<String, Object> ttmMap;
        ttmMap = Helpers.json2map(ttmJson.toString());
        log.info("\n.............. filter::mapdata ............. ="+ttmMap.toString());
        return ttmtableRepository.findAll();
    }

    /**
     * get one records from DB by ttmReference number
     * @param  ttmJson - json string
     * @return TTMTable obj as json
     * @throws None
     */
    @PostMapping("/get")
    public Optional<JPATTMTable> getOne(@RequestBody String ttmJson) {
        log.info("\n.............. jsondata ............ ="+ttmJson.toString());
        Map<String, Object> ttmMap;
        ttmMap = Helpers.json2map(ttmJson.toString());
        log.info("\n.............. mapdata ............. ="+ttmMap.toString());
        Optional<JPATTMTable> ttm = ttmtableRepository.findByTtmReference((String) ttmMap.get("ttmReference"));
        if (ttm == null){
            return null;
        }
        return ttm;
    }

    /**
     * update records in DB by ttmReference number
     * @param  ttmJson - json string
     * @return TTMTable obj as json otherwise TTMTable obj with Error message inside
     * @throws None
     */
    @PostMapping("/update")
    public Optional<JPATTMTable> updateTable(@RequestBody String ttmJson) {
        log.info("\n.............. jsondata ............ ="+ttmJson.toString());
        Map<String, Object> ttmMap;
        ttmMap = Helpers.json2map(ttmJson.toString());
        log.info("\n.............. mapdata ............. ="+ttmMap.toString());
        Optional<JPATTMTable> ttm = ttmtableRepository.findByTtmReference((String) ttmMap.get("ttmReference"));
        if (ttm == null){
            return null;
        }

        if (ttmMap.get("status") != null)
            ttm.get().status = (String) ttmMap.get("status");
        if (ttmMap.get("inizationalSystem") != null)
            ttm.get().inizationalSystem = (String) ttmMap.get("inizationalSystem");
        if (ttmMap.get("businessReference") != null)
            ttm.get().businessReference = (String) ttmMap.get("businessReference");
        if (ttmMap.get("externalId") != null)
            ttm.get().externalId = (String) ttmMap.get("externalId");
        if (ttmMap.get("senderBic") != null)
            ttm.get().senderBic = (String) ttmMap.get("senderBic");
        if (ttmMap.get("receiverBic") != null)
            ttm.get().receiverBic = (String) ttmMap.get("receiverBic");
        if (ttmMap.get("type") != null)
            ttm.get().type = (String) ttmMap.get("type");
        if (ttmMap.get("style") != null)
            ttm.get().style = (int) ttmMap.get("style");
        if (ttmMap.get("product") != null)
            ttm.get().product = (String) ttmMap.get("product");
        if (ttmMap.get("customerId") != null)
            ttm.get().customerId = (int) ttmMap.get("customerId");
        if (ttmMap.get("branchId") != null)
            ttm.get().branchId = (int) ttmMap.get("branchId");
        if (ttmMap.get("branchLocation") != null)
            ttm.get().branchLocation = (String) ttmMap.get("branchLocation");

        if (ttmMap.get("relatedTtmRef") != null)
            ttm.get().relatedTtmRef = (String) ttmMap.get("relatedTtmRef");
        if (ttmMap.get("mbe") != null)
            ttm.get().mbe = (String) ttmMap.get("mbe");
        if (ttmMap.get("systemId") != null)
            ttm.get().systemId = (String) ttmMap.get("systemId");
        if (ttmMap.get("tag20") != null)
            ttm.get().tag20 = (String) ttmMap.get("tag20");
        if (ttmMap.get("tag21") != null)
            ttm.get().tag21 = (String) ttmMap.get("tag21");
        if (ttmMap.get("trafinasBranchId") != null)
            ttm.get().trafinasBranchId = (String) ttmMap.get("trafinasBranchId");
        if (ttmMap.get("trafinasCaseId") != null)
            ttm.get().trafinasCaseId = (String) ttmMap.get("trafinasCaseId");
        if (ttmMap.get("trafinasEventId") != null)
            ttm.get().trafinasEventId = (String) ttmMap.get("trafinasEventId");
        if (ttmMap.get("trafinasEventId") != null)
            ttm.get().trafinasEventId = (String) ttmMap.get("trafinasEventId");
        if (ttmMap.get("otherSystem") != null)
            ttm.get().otherSystem = (String) ttmMap.get("otherSystem");
        if (ttmMap.get("doneAt") != null)
            ttm.get().doneAt = LocalDateTime.now();
        if (ttmMap.get("trafinasSendReqAt") != null)
            ttm.get().trafinasSendReqAt = LocalDateTime.parse((CharSequence)ttmMap.get("trafinasSendReqAt"));
        if (ttmMap.get("trafinasGetResAt") != null)
            ttm.get().trafinasSendReqAt = LocalDateTime.parse((CharSequence)ttmMap.get("trafinasGetResAt"));
        ttmtableRepository.save(ttm.get());
        return ttm;
    }
}
