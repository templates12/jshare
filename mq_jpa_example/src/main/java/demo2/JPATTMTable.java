package demo2;

/**
author: Kirill.Rozin@gmail.com
**/


import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class JPATTMTable extends JPABaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
   
    String status;
	String ttmReference;
	String inizationalSystem;
	String relatedTtmRef;
	String businessReference;
	String externalId;
	String senderBic;
	String receiverBic;
	String type;
	String product;
	String branchLocation;
	String mbe;
	String systemId;
	String tag20;
	String tag21;
	String trafinasBranchId;
	String trafinasCaseId;
	String trafinasEventId;
	String otherSystem;
	int style;
	int customerId;
	int branchId;
	LocalDateTime doneAt;
	LocalDateTime trafinasSendReqAt;
	LocalDateTime trafinasGetResAt;

	Long getId() {return id;}
}
