package demo2;

import java.util.Optional;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface JPATTMTableRepository extends PagingAndSortingRepository <JPATTMTable, Long> {
	Optional<JPATTMTable> findByTtmReference(String ttmReference);
}