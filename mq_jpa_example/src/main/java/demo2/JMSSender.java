package demo2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class JMSSender {
	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
    private JmsTemplate jmsTemplate;
	
	public void send(String message, String queuename) {
		log.info("sending message='{}'", message);
		jmsTemplate.convertAndSend(queuename, message);
	}
}