package demo2;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class JMSReceiver implements MessageListener {
	private final Logger log = LoggerFactory.getLogger(getClass());
	String latestMsg;

	@Autowired
    JmsTemplate jmsTemplate;

	@JmsListener(destination = "inbound.queue")
	public void onMessage(Message message) {
		if (message instanceof TextMessage) {
            try {
                String msg = ((TextMessage) message).getText();
                log.info("Message has been consumed : " + msg);
                System.out.println("Message has been consumed : " + msg);
                this.latestMsg = new String(msg);
            } catch (JMSException ex) {
                this.latestMsg = "JMSException";
            	throw new RuntimeException(ex);
            }
        } else {
            this.latestMsg = "IllegalArgumentException";
        	throw new IllegalArgumentException("Message Error");
        }
	}
	
	//@JmsListener(destination = "inbound.queue")
	//public String receiveMessage(Message message) throws JMSException {
	//	String txt = (String) jmsTemplate.receiveAndConvert();
	//	return txt;
	//}
}
