package demo2;

import static org.assertj.core.api.Assertions.assertThat;


import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import demo2.JPATTMTableRepository;
import demo2.JPATTMTable;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
@SpringBootTest(classes = Application.class)
@ContextConfiguration
class ApplicationTests {
	@Autowired
	private JPATTMTableRepository ttmtableRepository;

	@Autowired
	private JmsTemplate jmsTemplate;

	@Autowired
	private JMSSender sender;

	@Autowired
	private JMSReceiver receiver;
	
	@Test
	void contextLoads() {
		
	}
	
	@Test
	public void ttmtableTest() {
		JPATTMTable ttm = new JPATTMTable();
		ttm.ttmReference = Helpers.generateString(12);
		ttmtableRepository.save(ttm);
		
		Optional<JPATTMTable> persistedres = ttmtableRepository.findById(ttm.getId());
		System.out.println(persistedres.toString());
		System.out.println("Exists=" + persistedres.isPresent());
		System.out.println("createAt=" + persistedres.get().getCreateAt());
		
		Assert.assertTrue(persistedres.isPresent());
		Assert.assertNotNull(persistedres.get().ttmReference);
	}
	
    
	@Test
    public void testReceive() throws Exception {
    	sender.send("Hello Spring JMS ActiveMQ!", "inbound.queue");
    	TimeUnit.SECONDS.sleep(3);
    	System.out.println("\n============Receive message============  [" + receiver.latestMsg + "]\n");
    	assertThat(receiver.latestMsg).isEqualTo("Hello Spring JMS ActiveMQ!");
    }

}
