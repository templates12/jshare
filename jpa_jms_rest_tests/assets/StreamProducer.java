package demo2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

import demo2.ProducerChannels;

@EnableBinding(ProducerChannels.class)
public class StreamProducer {

	final MessageChannel broadcast, direct;

	@Autowired StreamProducer(ProducerChannels channels){
		this.broadcast = channels.broadcastPut();
		this.direct = channels.directPut();
	}
	
	boolean send2Queue(String message) {
		return this.direct.send(MessageBuilder.withPayload(message).build());
	}

	boolean send2Topic(String message) {
		return this.broadcast.send(MessageBuilder.withPayload(message).build());
	}
}
