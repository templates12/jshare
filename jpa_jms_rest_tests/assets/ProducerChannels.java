package demo2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.MessageChannel;


public interface ProducerChannels {
	
	String DIRECT = "directPut";
	String BROADCAST = "broadcastPut";
	
	@Output(DIRECT)
	MessageChannel directPut();
	
	@Output(BROADCAST)
	MessageChannel broadcastPut();
}
