package demo3;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JPATableUtils {
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private JPATableRepository ttmtableRepository;    
	
	Optional<JPATable> create() {
		log.debug("create");
		JPATable ttm = new JPATable();
		ttm.ttmReference = Helpers.generateString(12);
		ttmtableRepository.save(ttm);
	    ttm.setCreateAt(LocalDateTime.now());
	    ttmtableRepository.save(ttm);
	    return ttmtableRepository.findById(ttm.getId());
	}
	
    Iterable<JPATable> all() {
        return ttmtableRepository.findAll();
    }
    
    Optional<JPATable> oneOrNull(String ttmReference) {
        return ttmtableRepository.findByTtmReference(ttmReference);
    }
    
    Optional<JPATable> update(Map<String, Object> ttmMap) {
        log.info("\n update.mapdata="+ttmMap.toString());
        Optional<JPATable> ttm = ttmtableRepository.findByTtmReference((String) ttmMap.get("ttmReference"));
        if (ttm == null){
            return null;
        }
        System.out.println("\n TABLE before update="+ ttm.toString()+ "\n");
        
        if (ttmMap.get("status") != null)
            ttm.get().status = (String) ttmMap.get("status");
        if (ttmMap.get("inizationalSystem") != null)
            ttm.get().inizationalSystem = (String) ttmMap.get("inizationalSystem");
        if (ttmMap.get("businessReference") != null)
            ttm.get().businessReference = (String) ttmMap.get("businessReference");
        if (ttmMap.get("externalId") != null)
            ttm.get().externalId = (String) ttmMap.get("externalId");
        if (ttmMap.get("senderBic") != null)
            ttm.get().senderBic = (String) ttmMap.get("senderBic");
        if (ttmMap.get("receiverBic") != null)
            ttm.get().receiverBic = (String) ttmMap.get("receiverBic");
        if (ttmMap.get("type") != null)
            ttm.get().type = (String) ttmMap.get("type");
        if (ttmMap.get("style") != null)
            ttm.get().style = (int) ttmMap.get("style");
        if (ttmMap.get("product") != null)
            ttm.get().product = (String) ttmMap.get("product");
        if (ttmMap.get("customerId") != null)
            ttm.get().customerId = (int) ttmMap.get("customerId");
        if (ttmMap.get("branchId") != null)
            ttm.get().branchId = (int) ttmMap.get("branchId");
        if (ttmMap.get("branchLocation") != null)
            ttm.get().branchLocation = (String) ttmMap.get("branchLocation");

        if (ttmMap.get("relatedTtmRef") != null)
            ttm.get().relatedTtmRef = (String) ttmMap.get("relatedTtmRef");
        if (ttmMap.get("mbe") != null)
            ttm.get().mbe = (String) ttmMap.get("mbe");
        if (ttmMap.get("systemId") != null)
            ttm.get().systemId = (String) ttmMap.get("systemId");
        if (ttmMap.get("tag20") != null)
            ttm.get().tag20 = (String) ttmMap.get("tag20");
        if (ttmMap.get("tag21") != null)
            ttm.get().tag21 = (String) ttmMap.get("tag21");
        if (ttmMap.get("trafinasBranchId") != null)
            ttm.get().trafinasBranchId = (String) ttmMap.get("trafinasBranchId");
        if (ttmMap.get("trafinasCaseId") != null)
            ttm.get().trafinasCaseId = (String) ttmMap.get("trafinasCaseId");
        if (ttmMap.get("trafinasEventId") != null)
            ttm.get().trafinasEventId = (String) ttmMap.get("trafinasEventId");
        if (ttmMap.get("trafinasEventId") != null)
            ttm.get().trafinasEventId = (String) ttmMap.get("trafinasEventId");
        if (ttmMap.get("otherSystem") != null)
            ttm.get().otherSystem = (String) ttmMap.get("otherSystem");
        if (ttmMap.get("doneAt") != null)
            ttm.get().doneAt = LocalDateTime.now();
        if (ttmMap.get("trafinasSendReqAt") != null)
            ttm.get().trafinasSendReqAt = LocalDateTime.parse((CharSequence)ttmMap.get("trafinasSendReqAt"));
        if (ttmMap.get("trafinasGetResAt") != null)
            ttm.get().trafinasSendReqAt = LocalDateTime.parse((CharSequence)ttmMap.get("trafinasGetResAt"));

        ttmtableRepository.save(ttm.get());
        System.out.println("\n TABLE after update="+ ttm.toString()+ "\n");

        return ttmtableRepository.findByTtmReference((String) ttmMap.get("ttmReference"));
    }
}
