package demo3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;
import java.util.Optional;
import java.util.HashMap;

@RestController
@RequestMapping("/ttmtableapi")
public class RESTApiController {
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private JPATableUtils jpaTTMTableUtil;
	
	RESTApiController() {
		jpaTTMTableUtil = new JPATableUtils();
	}

    /**
     * get version
     * @param  None
     * @return json
     * @throws None
     */   
    @PostMapping("/ver")
    @ResponseBody
    public Map<String, Object> getVer() {
        Map<String, Object> reply = new HashMap<String, Object>();
        reply.put("version", "1.0");
        log.debug("getVer:" + reply.toString());
        return reply;
    }
	/*@PostMapping("/ver")
    @ResponseBody
    public ResponseEntity<?> getVer() {
        Map<String, Object> reply = new HashMap<String, Object>();
        reply.put("version", "1.0");
        log.debug("getVer:" + reply.toString());
        return ResponseEntity.ok(reply.toString());
    }*/    

    
    /**
     * create one records in DB
     * @param  ttmJson - json string
     * @return TTMTable obj as json
     * @throws None
     */
    @PostMapping("/new")
    public Optional<JPATable> setTable() {
    	return jpaTTMTableUtil.create();
    }

    /**
     * get all records from DB
     * @param  None
     * @return List
     * @throws None
     */
    @PostMapping("/all")
    public Iterable<JPATable> getAll() {
    	return jpaTTMTableUtil.all();
    }

    /**
     * get records from DB by filer
     * @param  json filter
     * @return List
     * @throws None
     */
    @PostMapping("/filter")
    public Iterable<JPATable> getByFilter(@RequestBody String ttmJson) {
        log.info("\n.............. filter::jsondata ............ ="+ttmJson.toString());
        Map<String, Object> ttmMap;
        ttmMap = Helpers.json2map(ttmJson.toString());
        log.info("\n.............. filter::mapdata ............. ="+ttmMap.toString());
        return jpaTTMTableUtil.all();
    }

    /**
     * get one records from DB by ttmReference number
     * @param  ttmJson - json string
     * @return TTMTable obj as json
     * @throws None
     */
    @PostMapping("/get")
    public Optional<JPATable> getOne(@RequestBody String ttmJson) {
        log.info("\n.............. jsondata ............ ="+ttmJson.toString());
        Map<String, Object> ttmMap;
        ttmMap = Helpers.json2map(ttmJson.toString());
        log.info("\n.............. mapdata ............. ="+ttmMap.toString());
        return jpaTTMTableUtil.oneOrNull((String) ttmMap.get("ttmReference"));
    }

    /**
     * update record by ttmReference number
     * @param  ttmJson - json string
     * @return TTMTable obj as json otherwise TTMTable obj with Error message inside
     * @throws None
     */
    @PostMapping("/update")
    public Optional<JPATable> updateTable(@RequestBody String ttmJson) {
        log.info("\n.............. jsondata ............ ="+ttmJson.toString());
        Map<String, Object> ttmMap;
        ttmMap = Helpers.json2map(ttmJson.toString());
        log.info("\n.............. mapdata ............. ="+ttmMap.toString());
        return jpaTTMTableUtil.update(ttmMap);
    }
}
