package demo3;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class JMSHandler {
	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private JMSSender sender;

	@Autowired
	JPATableUtils jpaTTMTableUtil;

	//@Value("${topic.name}")
	String topicname;

	//@Value("${topic.defaultfilter}")
	String topicdefaultfilter;

	@Autowired
	ConfigurationApplication jmsSettings;

    /**
     * take message from queue, handle it and push message/json to topic
     * @param  length int - requested lenght
     * @return String
     * @throws None
     */
	boolean queueMessageHandler(String message) {
		boolean result = false;
		String topic = jmsSettings.getTopicOutName();
		String filter = jmsSettings.getTopicDefaultFilter();
		String cmd = null;
		String reply = null;
		Optional<JPATable> table = null;

		log.debug("Request="+message);		
		Map<String, Object> req = Helpers.json2map(message);
        Map<String, Object> jheader = extractedHeader(req);
        Map<String, Object> jdata = extractedData(req);		
		System.out.println("\n queueMessageHandler.req = ["+ req +"]\n");
		if (req == null)
			return false;
		
		if (req.get("cmd") == null)
			return false;

		if (req.get("filer") != null)
			filter = (String) req.get("filer");

		cmd = (String) req.get("cmd");
        if( Arrays.asList("one","update").contains(cmd) && jdata == null) {
            log.error("queueMessageHandler data = null");
            System.out.println("\n queueMessageHandler data = null\n");
            return false;
        }
		
		switch(cmd) {
			case "ver":
		        Map<String, Object> res = new HashMap<String, Object>();
		        res.put("version", "1.0");
				reply = Helpers.map2json(res);
				result = true;
				break;
			case "new":
				table = jpaTTMTableUtil.create();
				System.out.println("\n TABLE="+ table.toString()+ "\n");
				reply = Helpers.pojo2json(table.get());
				result = true;
				break;
			case "update":
				table = jpaTTMTableUtil.update(extractedData(req));
				if (table == null) {
					result = false;
					break;
				}
				reply = Helpers.pojo2json(table.get());
				result = true;
				break;
			case "one":
				table = jpaTTMTableUtil.oneOrNull((String)extractedData(req).get("ttmReference"));
				if (table == null) {
					result = false;
					break;
				}				
				reply = Helpers.pojo2json(table.get());
				result = true;
				break;
		}
		log.debug("Reply="+reply);
		System.out.println("\n queueMessageHandler.Reply=["+ reply +"]\n");

        if (result == false)
            return false;

		sender.send2topic(reply, topic, filter);
		return result;
	}

    private Map<String, Object> extractedData(Map<String, Object> req) {
        if (req == null)
            return null;
        return (Map<String, Object>) req.get("data");
    }

    private Map<String, Object> extractedHeader(Map<String, Object> req) {
        if (req == null)
            return null;
        return (Map<String, Object>) req.get("header");
    }
}
