package demo3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class JMSSender {
	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
    private JmsTemplate jmsTemplate;
	
	public void send2queue(String message, String queuename) {
		log.info("send: sending message='{}'", message);
		jmsTemplate.convertAndSend(queuename, message);
	}
	
	public void send2topic(String message, String topicname, String filter) {
		log.info("send2topic: sending message='{}'", message);
		jmsTemplate.convertAndSend(topicname, message,
		          messagePostProcessor -> {
		        	  messagePostProcessor.setStringProperty("filter", filter);
		        	  return messagePostProcessor;});
	}
}