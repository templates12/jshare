package demo3;

import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.security.SecureRandom;
import java.io.IOException;
import java.util.Map;

import org.json.JSONObject;
import org.json.JSONTokener;

import com.fasterxml.jackson.databind.ObjectMapper;


public class Helpers {
    /**
     * Generate a random String with requested lenght
     * @param  length int - requested lenght
     * @return String
     * @throws None
     */

	public static String generateString(int length) {
		//Random random = new Random();
        try {
            Random rand = SecureRandom.getInstanceStrong();
            final String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            char[] text = new char[length];
            for (int i = 0; i < length; i++) {
                text[i] = characters.charAt(rand.nextInt(characters.length()));
            }
            return new String(text);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "ERROR";
    }
	
    /**
     * ORM from String json to corresponding Java Object
     * @param  input   json string
     * @return HashMap java object
     * @throws IOException
     */
    public static Map<String, Object> json2map(final String input) {
        JSONTokener parser = new JSONTokener(input);
        JSONObject json = new JSONObject(parser);
        return json.toMap();
    }

    /**
     * ORM from Java HashMap to json string
     * @param  obj   HashMap
     * @return java string
     */
    public static String map2json(final Map<String, Object> obj) {
        JSONObject jobj = new JSONObject(obj);
        return jobj.toString();
    }
    
    /**
     * convert from POJO to Map
     * @param  obj   POJO Object
     * @return Map
     */
    public static Map<String, Object> pojo2map(final Object obj) {
    	ObjectMapper oMapper = new ObjectMapper();
        @SuppressWarnings("unchecked")
		Map<String, Object> reply = oMapper.convertValue(obj, Map.class);
        return reply;
    }
    
    /**
     * convert from POJO to json string
     * @param  obj   POJO Object
     * @return String
     */
    public static String pojo2json(final Object obj) {
    	Map<String, Object> reply = pojo2map(obj);
        return map2json(reply);
    }    
}