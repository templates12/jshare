package demo3;

import java.util.Optional;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface JPATableRepository extends PagingAndSortingRepository <JPATable, Long> {
	Optional<JPATable> findByTtmReference(String ttmReference);
}