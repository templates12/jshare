package demo3;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
// import org.springframework.beans.factory.annotation.Value;

@Service
public class JMSReceiver implements MessageListener {
	private final Logger log = LoggerFactory.getLogger(getClass());
	String latestMsgInQueue;
	String latestMsgInTopic;

	@Autowired
    JmsTemplate jmsTemplate;
	
	@Autowired
	JMSHandler jmsManager;	

	@Autowired
	ConfigurationApplication jmsSettings;
	
	@JmsListener(destination = "${jmssettings.queueInName}")
	public void onMessage(Message message) {
		String msg = null;
		try {
			msg = ((TextMessage) message).getText();
		} catch (JMSException e) {
			e.printStackTrace();
			this.latestMsgInQueue = null;
		}
		log.info("onMessage: >>> Message has been consumed : " + msg);
        System.out.println("onMessage: Message has been consumed : " + msg);        
        this.latestMsgInQueue = msg;
        jmsManager.queueMessageHandler(msg);
	}

	/* TESTING GOAL
	@JmsListener(destination = "${jmssettings.topicOutName}", selector = "filter = 'all'")
	public void onMessageTopic(Message message) throws JMSException {
		String msg = null;
		try {
			msg = ((TextMessage) message).getText();
		}
		catch (JMSException e) {
			e.printStackTrace();
			this.latestMsgInQueue = null;
		}
		log.info("onMessageTopic: >>> Message has been consumed : " + msg);
        System.out.println("onMessageTopic: Message has been consumed : " + msg);
        this.latestMsgInTopic = msg;
	}*/

	public String receive4queue(String queue) {
		String msg = (String) jmsTemplate.receiveAndConvert(queue);
		log.info("receive4queue: >>> Message has been consumed : " + msg);
        System.out.println("receive4queue: Message has been consumed : " + msg);		
        this.latestMsgInQueue = msg;
		return msg;
	}
	
	public String receive4topic(String topic) {
		String msg = (String) jmsTemplate.receiveAndConvert(topic);
		log.info("receive4queue: >>> Message has been consumed : " + msg);
        System.out.println("receive4queue: Message has been consumed : " + msg);		
        this.latestMsgInTopic = msg;
        return msg;
	}
}
