package demo3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;

@SpringBootApplication
@EnableJms
@EnableConfigurationProperties
public class Application {
	static JmsTemplate jmsTemplate;
	static JPATableRepository ttmtableRepository;
	static ConfigurationApplication jsmSettings;
	
	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
		jmsTemplate = context.getBean(JmsTemplate.class);
		ttmtableRepository = context.getBean(JPATableRepository.class);
		jsmSettings = context.getBean(ConfigurationApplication.class);
		
		System.out.println("Confihuration = " + jsmSettings.toString());
	}
}