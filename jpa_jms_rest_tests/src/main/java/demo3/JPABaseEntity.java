package demo3;

import lombok.Data;

import javax.persistence.MappedSuperclass;

import java.time.LocalDateTime;

import javax.persistence.EntityListeners;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class JPABaseEntity {
	@CreatedDate
	protected LocalDateTime createAt;
	
	@LastModifiedDate
	protected LocalDateTime lastModified;
	
	LocalDateTime getCreateAt() {return createAt;}
	void setCreateAt(LocalDateTime createAt) {this.createAt = createAt;}	
}
