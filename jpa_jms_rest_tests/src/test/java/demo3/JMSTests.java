package demo3;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import demo3.Helpers;
import demo3.JMSReceiver;
import demo3.JMSSender;
import demo3.JPATable;
import demo3.JPATableRepository;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
@SpringBootTest
@ContextConfiguration
class JMSTests {
	@Autowired
	private JPATableRepository ttmtableRepository;

	@Autowired
	private JMSSender sender;

	@Autowired
	private JMSReceiver receiver;
	
	@Value("${jmssettings.queueInName}")
	String queuename;

	@Autowired
	ConfigurationApplication jmsSettings;

	@Before
	public void setUp() throws Exception {
		receiver.latestMsgInQueue=null;
		receiver.latestMsgInTopic=null;
		ttmtableRepository.deleteAll();
	}

	@Test
	@Order(1)	
    public void testSendReceiveQueue() throws Exception {
		Map<String, Object> req = new HashMap<String, Object>();
		req.put("cmd", "ver");
		req.put("filer", "all");
		req.put("test", "testSendReceiveQueue");
		String sendMsg = Helpers.map2json(req);

		Map<String, Object> reply = new HashMap<String, Object>();
		reply.put("cmd", "ver");
		reply.put("filer", "all");
		reply.put("test", "testSendReceiveQueue");
		String getMsg = Helpers.map2json(reply);
		
		System.out.println("\n testSendReceiveQueue.sendMsg=[" + sendMsg + "]\n");
		sender.send2queue(sendMsg, jmsSettings.getTestQueueName());
    	TimeUnit.SECONDS.sleep(3);

    	String msg = receiver.receive4queue(jmsSettings.getTestQueueName());
    	System.out.println("\n testSendReceiveQueue.Receive message=[" + msg + "]\n");
    	assertThat(msg).isEqualTo(getMsg);
    }


	@Test
	@Order(2)	
    public void testSendReceiveTopic() throws Exception {
		Map<String, Object> req = new HashMap<String, Object>();
		req.put("cmd", "ver");
		req.put("filer", "all");
		req.put("test", "testSendReceiveTopic");
		String sendMsg = Helpers.map2json(req);

		Map<String, Object> reply = new HashMap<String, Object>();
		reply.put("cmd", "ver");
		reply.put("filer", "all");
		reply.put("test", "testSendReceiveTopic");		
		String getMsg = Helpers.map2json(reply);

		System.out.println("\n testSendReceiveTopic.sendMsg=[" + sendMsg + "]\n");
		sender.send2topic(sendMsg, jmsSettings.getTestTopicName(), jmsSettings.getTopicDefaultFilter());
    	TimeUnit.SECONDS.sleep(3);
    	
    	String msg = receiver.receive4queue(jmsSettings.getTestTopicName());
    	System.out.println("\n testSendReceiveTopic.Receive message=[" + msg + "]\n");
    	assertThat(msg).isEqualTo(getMsg);
    }
	
	// @Disabled("Not ready yet")
	@Test
	@Order(3)	
    public void testJMSManagerVer() throws Exception {
		Map<String, Object> req = new HashMap<String, Object>();
		req.put("cmd", "ver");
		req.put("filer", "all");
		String sendMsg = Helpers.map2json(req);
	
		sender.send2queue(sendMsg, jmsSettings.getQueueInName());
		System.out.println("\n IN = []" + jmsSettings.getQueueInName() + " testJMSManagerVer.sendMsg=[" + sendMsg + "]\n");
		TimeUnit.SECONDS.sleep(3);

    	String msg = receiver.receive4topic(jmsSettings.getTopicOutName());
        System.out.println("\n testJMSManagerVer.Receive message=[" + msg + "]\n");        

        Map<String, Object> reply = new HashMap<String, Object>();
		reply = Helpers.json2map(msg);		
    	assertNotNull(reply.get("version"));
    }
	
	@Test
	@Order(4)	
    public void testJMSManagerNew() throws Exception {
		Map<String, Object> req = new HashMap<String, Object>();
		req.put("cmd", "new");
		req.put("filer", "all");
		String sendMsg = Helpers.map2json(req);
		
		sender.send2queue(sendMsg, jmsSettings.getQueueInName());
    	TimeUnit.SECONDS.sleep(1);
		System.out.println("\n IN = []" + jmsSettings.getQueueInName() + " testJMSManagerNew.sendMsg=[" + sendMsg + "]\n");

    	String msg = receiver.receive4topic(jmsSettings.getTopicOutName());
        System.out.println("\n testJMSManagerNew.Receive message=[" + msg + "]\n");

    	Map<String, Object> reply = new HashMap<String, Object>();
		reply = Helpers.json2map(msg);		
    	assertNotNull(reply.get("ttmReference"));
    }

	@Test
	@Order(5)	
    public void testJMSManagerOne() throws Exception {
		JPATable ttm = new JPATable();
		ttm.ttmReference = Helpers.generateString(12);
		ttmtableRepository.save(ttm);

		Map<String, Object> jdata = new HashMap<String, Object>();
		jdata.put("ttmReference", ttm.ttmReference);

		Map<String, Object> req = new HashMap<String, Object>();
		req.put("cmd", "one");
		req.put("filer", "all");
		req.put("data", jdata);		
		String sendMsg = Helpers.map2json(req);
	
		sender.send2queue(sendMsg, jmsSettings.getQueueInName());
		System.out.println("\n IN = []" + jmsSettings.getQueueInName() + " testJMSManagerOne.sendMsg=[" + sendMsg + "]\n");		
    	TimeUnit.SECONDS.sleep(3);
    	
    	String msg = receiver.receive4topic(jmsSettings.getTopicOutName());
        System.out.println("\n testJMSManagerOne.Receive message=[" + msg + "]\n");

		Map<String, Object> reply = new HashMap<String, Object>();
		reply = Helpers.json2map(receiver.latestMsgInTopic);
    	assertNotNull(reply.get("ttmReference"));
    	assertThat(ttm.ttmReference).isEqualTo(reply.get("ttmReference"));
    }
	
	@Test
	@Order(6)	
    public void testJMSManagerUpdate() throws Exception {
		JPATable ttm = new JPATable();
		ttm.ttmReference = Helpers.generateString(12);
		ttm.status = "INIT";
		ttmtableRepository.save(ttm);

		Map<String, Object> jdata = new HashMap<String, Object>();
		jdata.put("ttmReference", ttm.ttmReference);
		jdata.put("status", "foobarbaz");

		Map<String, Object> req = new HashMap<String, Object>();
		req.put("cmd", "update");
		req.put("filer", "all");
		req.put("data", jdata);
		
		String sendMsg = Helpers.map2json(req);		
		sender.send2queue(sendMsg, jmsSettings.getQueueInName());
		System.out.println("\n IN = []" + jmsSettings.getQueueInName() + " testJMSManagerUpdate.sendMsg=[" + sendMsg + "]\n");
		
    	TimeUnit.SECONDS.sleep(3);
    	String msg = receiver.receive4topic(jmsSettings.getTopicOutName());
        System.out.println("\n testJMSManagerUpdate.Receive message=[" + msg + "]\n");

		Map<String, Object> reply = new HashMap<String, Object>();
		reply = Helpers.json2map(receiver.latestMsgInTopic);
		
		System.out.println("\n\\n testJMSManagerUpdate.reply=[" + reply + "]\n");
    	assertNotNull(reply.get("ttmReference"));
    	assertThat(ttm.ttmReference).isEqualTo((String) reply.get("ttmReference"));    	
    	assertThat((String) reply.get("status")).isEqualTo("foobarbaz");
    }		
}
