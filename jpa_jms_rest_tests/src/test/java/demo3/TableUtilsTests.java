package demo3;


import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import demo3.JPATable;
import demo3.JPATableRepository;
import demo3.JPATableUtils;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
@SpringBootTest
@ContextConfiguration
class TableUtilsTests {
    MockitoSession session;
    
    @Mock
	private JPATableRepository testRepository;

	@Autowired
	JPATableUtils jpaTTMTableUtil;
	
	@Before
	public void setUp() throws Exception {
		testRepository.deleteAll();
		jpaTTMTableUtil = new JPATableUtils();
        session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
	}

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void testCreate() {
    	JPATableUtils util = Mockito.mock(JPATableUtils.class);
        util.create();
        Mockito.verify(util, Mockito.times(1)).create();
    }

    @Test
    public void testUpdate() throws Exception {
    	Optional<JPATable> ttm = jpaTTMTableUtil.create();

        Map<String, Object> ttmMap = new HashMap<String, Object>();
        ttmMap.put("ttmReference", ttm.get().ttmReference);
        ttmMap.put("status", "foobarbaz");        
        Optional<JPATable> ttm2 = jpaTTMTableUtil.update(ttmMap);
        assertEquals(ttm2.get().status, "foobarbaz");
        
        JPATableUtils util = Mockito.mock(JPATableUtils.class);
        Mockito.when(util.update(any())).thenReturn(ttm2);
        util.update(ttmMap);
        Mockito.verify(util).update(any());
    }
}
