package demo3;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
//import org.junit.Ignore;
import org.junit.runner.RunWith;
//import static org.mockito.Mockito.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import demo3.Helpers;
import demo3.JPATable;
import demo3.JPATableRepository;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
@SpringBootTest
@ContextConfiguration
class JPATests {
	@Autowired
	private JPATableRepository ttmtableRepository;

	@Before
	public void setUp() throws Exception {
		ttmtableRepository.deleteAll();
	}
	
	@Test
	public void ttmtableTest() {
		JPATable ttm = new JPATable();
		ttm.ttmReference = Helpers.generateString(12);
		ttmtableRepository.save(ttm);

		Optional<JPATable> persistedres = ttmtableRepository.findById(ttm.getId());
		System.out.println(persistedres.toString());
		System.out.println("Exists=" + persistedres.isPresent());
		System.out.println("createAt=" + persistedres.get().getCreateAt());
		
		Assert.assertTrue(persistedres.isPresent());
		Assert.assertNotNull(persistedres.get().ttmReference);
	}

}
