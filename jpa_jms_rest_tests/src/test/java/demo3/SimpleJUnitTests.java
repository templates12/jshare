package demo3;

import static org.junit.jupiter.api.Assertions.*;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class SimpleJUnitTests {
	@BeforeAll
	public static void setUpClassOnce() {
		System.out.println("This @BeforeAll will be invoked one time before all test");
	}
	
	@AfterAll
	public static void tearDownClassOnce() {
		System.out.println("This @AfterAll will be invoked one time after all test");
	}

	@BeforeEach
	public void setUpTestData() {
		System.out.println("This @BeforeEach will be invoked one time before each test");
	}
	
	@AfterEach
	public void tearDownTestData() {
		System.out.println("This @AfterEach will be invoked one time after each test");
	}
	
    @Disabled("for demonstration purposes")
	@Test
    void testSkippedTest() {
    	System.out.println("@Disabled just for testing purpose");
    	assertFalse(true);
    }

	@ParameterizedTest
	@ValueSource(strings = { "ttt" })
	void testCheckString(String x) {
	    assertTrue("ttt".equals(x));
	}
	
	@Test
	@Tag(value="long")
	@Timeout(value = 4000)
	@Order(2)
	@DisplayName("testDoTimeout2")	
	public void testDoTimeout2() {
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	@Disabled("disabled for testing purpose")
	void testFail() {
		fail("Not yet implemented");
	}

}
