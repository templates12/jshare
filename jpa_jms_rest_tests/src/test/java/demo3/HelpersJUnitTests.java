package demo3;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;


/*
 * useful url: 
 * https://junit.org/junit5/docs/current/user-guide/
 * 
 * run: mvn clean compile -Dtest=HelpersJUnitTests#test* test
 * 
 * */

public class HelpersJUnitTests {
	private Map testmap = new HashMap();
	
	
	@BeforeAll
	public static void setUpClassOnce() {
		System.out.println("This @BeforeAll will be invoked one time before all test");
	}
	
	@AfterAll
	public static void tearDownClassOnce() {
		System.out.println("This @AfterAll will be invoked one time after all test");
	}

	@BeforeEach
	public void setUpTestData() {
		System.out.println("This @BeforeEach will be invoked one time before each test");
		testmap.put("version", "1.0");
	}
	
	@AfterEach
	public void tearDownTestData() {
		System.out.println("This @AfterEach will be invoked one time after each test");
		testmap.clear();
	}
	
	@Test
	@Timeout(value = 1000)
	@RepeatedTest(3)	
	@DisplayName("testCheckMap2Json")
	public void testCheckMap2Json() {
		String msg = Helpers.map2json(testmap);
		System.out.println("converted = " + msg);
		assertThat(msg).isEqualTo("{\"version\":\"1.0\"}");
	}
	
	@Test
	@Timeout(value = 1000)
	@RepeatedTest(3)	
	@DisplayName("testCheckMap2Json")
	public void testCheckJson2Map() {
		String reply = "{\"version\":\"1.0\"}";
		Map replymap = Helpers.json2map(reply);
		System.out.println("converted = " + replymap);
		assertThat(replymap).isEqualTo(testmap);
	}	
	
}
