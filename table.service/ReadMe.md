#Table Java 8 Service
Table create/read/update information in Table


Description
===========
service supports:
1. tableapi/new - POST - create a new one
2. tableapi/get - POST - get one by ttm_reference
3. tableapi/update - POST - update one by ttm_reference

All URLs support POST request and all data shall be passed through JSON data

Configuration
=============
see spring documentation

### OS variables:

na
