package table.service;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class JPATableUtils {
	public static final String LIMIT = "limit";
	private static final String STARTDATE = "startDate";
	private static final String STATUS = "status";
	private static final String REFERENCE = "reference";
	public static final String CREATED_AT = "createdAt";
	public static final String CREATED_AT_DATE = "createdAtDate";
	public static final String ORDER_BY = "orderby";
	public static final String PRIORITY = "priority";

	private Instant startMethodTime;
	private Instant endMethodTime;

	@Autowired private JPATableRepository tableRepository;

	Optional<JPATable> create(Map<String, Object> tMap) {
		log.debug(Helpers.getInternalLogMessage("create record"));
		JPATable tbl = new JPATable();
		startMethodTime = Instant.now();
		if (tMap == null)
			tbl.reference = Helpers.generateString(12);
		else {
			if (tMap.get(REFERENCE) != null)
				tbl.reference = (String) tMap.get(REFERENCE);
			else
				tbl.reference = Helpers.generateString(12);

			if (tMap.get(STARTDATE) != null)
				tbl.startDate = (String) tMap.get(STARTDATE);
			if (tMap.get(STATUS) != null)
				tbl.status = (String) tMap.get(STATUS);
			if (tMap.get(PRIORITY) != null)
				tbl.priority = (int) tMap.get(PRIORITY);
		}
		tableRepository.save(tbl);
		log.info(Helpers.getInternalLogMessage("Entry with reference=\"" + tbl.reference + "\" has been added with status=" + tbl.status));
		endMethodTime = Instant.now();
		log.info(Helpers.getInternalLogMessage("Duration time for creating new record in Table:" + Duration.between(startMethodTime, endMethodTime)));
		return tableRepository.findById(tbl.getId());
	}

	Iterable<JPATable> all() {
		startMethodTime = Instant.now();
		Iterable<JPATable> resultList = tableRepository.findAll();
		endMethodTime = Instant.now();
		log.info(Helpers.getInternalLogMessage("Duration time for getting all results from Table:" + Duration.between(startMethodTime, endMethodTime)));
		return resultList;
	}

	Optional<JPATable> oneOrNull(String reference) {
		return tableRepository.findByReference(reference);
	}

	Optional<JPATable> update(Map<String, Object> tMap) {
		log.info(Helpers.getInternalLogMessage("update.mapdata=" + tMap.toString()));
		Optional<JPATable> tbl = tableRepository.findByReference((String) tMap.get(REFERENCE));
		if (!tbl.isPresent()) {
			log.warn(Helpers.getInternalLogMessage("there is no reference=" + tMap.get(REFERENCE)));
			return Optional.ofNullable(null);
		}
		startMethodTime = Instant.now();
		log.debug(Helpers.getInternalLogMessage("TABLE before update=" + tbl.toString()));

		if (tMap.get(STARTDATE) != null)
			tbl.get().startDate = (String) tMap.get(STARTDATE);
		if (tMap.get(STATUS) != null)
			tbl.get().status = (String) tMap.get(STATUS);
		if (tMap.get(PRIORITY) != null)
			tbl.get().priority = (int) tMap.get(PRIORITY);
		if (tMap.get("doneDate") != null)
			tbl.get().doneDate = LocalDateTime.now();

		tableRepository.save(tbl.get());
		log.debug(Helpers.getInternalLogMessage("TABLE after update=" + tbl.toString()));
		log.info(Helpers.getInternalLogMessage("Entry with reference=\"" + tMap.get(REFERENCE) + "\" has been updated with status=" + tMap.get(STATUS)));
		log.info(Helpers.getInternalLogMessage("TABLE has been updated by refeence=" + tMap.get(REFERENCE)));
		log.info(Helpers.getInternalLogMessage("Duration time for updating Table:" + Duration.between(startMethodTime, Instant.now())));
		return tableRepository.findByReference((String) tMap.get(REFERENCE));
	}
	
	List<JPATable> filter(Map<String, Object> tMap) {
		log.debug(Helpers.getInternalLogMessage("filter data =" + tMap.toString()));
		List<JPATable> resultList;
		startMethodTime = Instant.now();
		if (tMap.get(STATUS) != null) {
			resultList = Lists.newArrayList(tableRepository.findByStatus((String) tMap.get(STATUS)));
			endMethodTime = Instant.now();
			log.info(Helpers.getInternalLogMessage("Duration time for query findByTypeAndStatus:" + Duration.between(startMethodTime, endMethodTime)));
			return resultList;
		} 
		else if (tMap.get(REFERENCE) != null && tMap.get(STATUS) != null) {
			List<JPATable> data = new ArrayList<>();
			Optional<JPATable> trec = tableRepository.findByStatusAndReference((String) tMap.get(STATUS), (String) tMap.get(REFERENCE));
			if (trec.isPresent()) {
				JPATable tEntity = trec.get();
				if (tEntity != null)
					data.add(tEntity);
			}
			endMethodTime = Instant.now();
			log.info(Helpers.getInternalLogMessage("Duration time for query findByTypeAndreference:" + Duration.between(startMethodTime, endMethodTime)));
			return data;
		}
		endMethodTime = Instant.now();
		log.info(Helpers.getInternalLogMessage("Duration time for empty query in method filter:" + Duration.between(startMethodTime, endMethodTime)));
		return Collections.emptyList();
	}

	List<JPATable> filterByOrderByLimitTo1(Map<String, Object> tMap) {
		startMethodTime = Instant.now();
		log.debug(Helpers.getInternalLogMessage("filter data =" + tMap.toString()));
		//find by status
		if (tMap.get(STATUS) != null && tMap.get(ORDER_BY) != null && tMap.get(LIMIT) != null && (int) tMap.get(LIMIT) == 1) {
			List<JPATable> resultList = Lists.newArrayList(tableRepository.findFirstByStatusOrderByCreatedAtDesc((String) tMap.get(STATUS)));
			//Optional<JPATable> trec = tableRepository.findFirstByStatusOrderByCreatedAtDesc((String) tMap.get(STATUS));
			log.info(Helpers.getInternalLogMessage("Duration time for query findFirstByStatusOrderByCreatedAtDesc:" + Duration.between(startMethodTime, Instant.now())));
			return resultList;
		}
		//find by priority
		if (tMap.get(PRIORITY) != null && tMap.get(ORDER_BY) != null && tMap.get(LIMIT) != null && (int)tMap.get(LIMIT) == 1) {
			List<JPATable> resultList = Lists.newArrayList(tableRepository.findFirstByPriorityOrderByCreatedAtDesc((int) tMap.get(PRIORITY)));
			//Optional<JPATable> trec = tableRepository.findFirstByPriorityOrderByCreatedAtDesc((int) tMap.get(PRIORITY));
			log.info(Helpers.getInternalLogMessage("Duration time for query findFirstByPriorityOrderByCreatedAtDesc:" + Duration.between(startMethodTime, Instant.now())));
			return resultList;
		}
		//find by priority and status
		if (tMap.get(STATUS) != null && tMap.get(PRIORITY) != null && tMap.get(ORDER_BY) != null && tMap.get(LIMIT) != null && (int)tMap.get(LIMIT) == 1) {
			List<JPATable> resultList = Lists.newArrayList(tableRepository.findFirstByStatusAndPriorityOrderByCreatedAtDesc((String) tMap.get(STATUS), (int) tMap.get(PRIORITY)));
			//Optional<JPATable> trec = tableRepository.findFirstByStatusAndPriorityOrderByCreatedAtDesc((String) tMap.get(STATUS), (int) tMap.get(PRIORITY));
			log.info(Helpers.getInternalLogMessage("Duration time for query findFirstByStatusAndPriorityOrderByCreatedAtDesc:" + Duration.between(startMethodTime, Instant.now())));
			return resultList;
		}
		log.info(Helpers.getInternalLogMessage("Duration time for empty query filterByOrderByLimitTo1:"	+ Duration.between(startMethodTime, Instant.now())));
		//return Collections.emptyMap();
		return Collections.emptyList();
	}
}
