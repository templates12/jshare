package table.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan
@EnableJms
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
public class Application {
    static JmsTemplate jmsTemplate;
    static JmsTemplate jmsTemplateTopic;
    static JPATableRepository ttmtableRepository;
    static ConfigurationJMS jsmSettings;
    static ConfigurationService serviceSettings;

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
        ttmtableRepository = context.getBean(JPATableRepository.class);
        jsmSettings = context.getBean(ConfigurationJMS.class);
        serviceSettings = context.getBean(ConfigurationService.class);

        jmsTemplate = context.getBean(JmsTemplate.class);
        jmsTemplate.setPubSubDomain(false);
        
        jmsTemplateTopic = context.getBean(JmsTemplate.class);
        jmsTemplateTopic.setPubSubDomain(true);
    }

    /**
     * Required for JPA auditing
     * @return auditor aware
     */
    @Bean
    public AuditorAware<String> auditorAware() {
        return new AuditorAwareImpl();
    }

}

