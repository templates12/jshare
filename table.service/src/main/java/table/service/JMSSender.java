package table.service;

import java.util.Map;

import javax.jms.Message;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class JMSSender {

	@Autowired	private JmsTemplate jmsTemplate;
	@Autowired	private JmsTemplate jmsTemplateTopic;
	@Autowired	ConfigurationService serviceSettings;
	@Autowired	ConfigurationJMS jmsSettings;

	/**
	 * send message to destination synchronously
	 * 
	 * @param message   String message
	 * @param queuename String - name of queue
	 * @return void
	 * @throws None
	 */
	public void send2queue(String message, String queuename) {
		log.info(Helpers.getExternalLogMessage(false, "sending message=" + message));
		jmsTemplate.setPubSubDomain(false);
		jmsTemplate.convertAndSend(queuename, message);
	}

	/**
	 * send message to destination synchronously
	 * 
	 * @param message     String message
	 * @param destination String - name of queue or topic
	 * @param filter      Map - jsm header info
	 * @return void
	 * @throws None
	 */
	public void send2topic(String message, String destination, String filter) {
		log.info(Helpers.getExternalLogMessage(false, "sending message=" + message));
		
		if (filter == null) {
			jmsTemplateTopic.convertAndSend(destination, message);
		} else {
			jmsTemplateTopic.convertAndSend(destination, message, messagePostProcessor -> {
				messagePostProcessor.setStringProperty("filter", filter);
				return messagePostProcessor;
			});
		}
	}

	/**
	 * send message to queue synchronously
	 * 
	 * @param message     String message
	 * @param destination String - name of queue or topic
	 * @param jmsHeader   Map - jsm header info
	 * @return true or false
	 * @throws None
	 */
	public boolean send2queue(String msg, String destination, Map<String, Object> jmsHeader) {
		log.info(Helpers.getExternalLogMessage(false, "send2queue: sending destination=" + destination + "\t jmsHeader="+ jmsHeader.toString() + "\t message = " + msg));
		if (jmsHeader == null)
			return false;
		jmsTemplate.setPubSubDomain(false);
		return sendMessage(msg, destination, jmsHeader);
	}

	private boolean sendMessage(String msg, String destination, Map<String, Object> jmsHeader) {
		jmsTemplate.send(destination, session -> {
			Message message = getRequiredMessageConverter().toMessage(msg, session);
			if (jmsHeader.get("jmsMessageID") != null)
				message.setJMSMessageID((String) jmsHeader.get("jmsMessageID"));
			if (jmsHeader.get("jmsCorrelationID") != null)
				message.setJMSCorrelationID((String) jmsHeader.get("jmsCorrelationID"));
			if (jmsHeader.get("jmsPriority") != null)
				message.setJMSPriority((int) jmsHeader.get("jmsPriority"));
			if (jmsHeader.get("jmsDeliveryMode") != null)
				message.setJMSDeliveryMode((int) jmsHeader.get("jmsDeliveryMode"));
			if (jmsHeader.get("jmsType") != null)
				message.setJMSType((String) jmsHeader.get("jmsType"));
			if (jmsHeader.get("jmsExpiration") != null)
				message.setJMSExpiration((long) jmsHeader.get("jmsExpiration"));
			if (jmsHeader.get("jmsRedelivered") != null)
				message.setJMSRedelivered((boolean) jmsHeader.get("jmsRedelivered"));
			if (jmsHeader.get("jmsTimestamp") != null)
				message.setJMSTimestamp((long) jmsHeader.get("jmsTimestamp"));
			if (jmsHeader.get("jmsDestination") != null)
				message.setJMSDestination(session.createQueue((String) jmsHeader.get("jmsDestination")));
			if (jmsHeader.get("jmsReplyTo") != null)
				message.setJMSReplyTo(session.createQueue((String) jmsHeader.get("jmsReplyTo")));
			return message;
		});
		return true;
	}

	/** Spring JmsTemplate library */
	private MessageConverter getRequiredMessageConverter() {
		try {
			return jmsTemplate.getMessageConverter();
		} catch (Exception e) {
			log.error("No 'messageConverter' specified. Check configuration of JmsTemplate. {0}", e);
		}
		return null;
	}
}
