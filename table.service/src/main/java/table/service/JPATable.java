package table.service;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@EntityListeners(AuditingEntityListener.class)
public class JPATable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;


	@Column(unique = true)
	String reference = "";

	String startDate = "";
	String status = "";
	int priority = -1;
	LocalDateTime doneDate;

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Europe/Zagreb")
	@CreatedDate
	Date createdAt;
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Europe/Zagreb")
	@LastModifiedDate
	Date lastModified;
	//Date for GUI
	@JsonFormat(pattern="yyyy-MM-dd")
	@CreatedDate
	@Temporal(TemporalType.DATE)
	Date createdAtDate;

	Long getId() {
		return id;
	}

}
