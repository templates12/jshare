package table.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class JMSHandler {
	private static final String FILTER = "filter";
	private static final String FILTERBYORDERBY = "filterbyorderby";
	private static final String REPLYTO = "replyto";
	private static final String QUEUE = "queue";
	private static final String QUEUE_OR_TOPIC = "queueORtopic";
	private static final String HEADER = "header";
	private static final String CMD = "cmd";
	private static final String ONE = "one";
	private static final String UPDATE = "update";
	private static final String VER = "ver";
	private static final String NEW = "new";
	private static final String SERVICE_REPLY = "serviceReply";
	private static final String REFERENCE = "reference";
	public static final String LIMIT = "limit";
	

	@Autowired ConfigurationService serviceSettings;
	@Autowired ConfigurationJMS jmsSettings;
	@Autowired private JMSSender sender;
	@Autowired JPATableUtils jpaTableUtil;

	/**
	 * take message from queue, handle it and push message/json to topic
	 * 
	 * @param length int - requested lenght
	 * @return String
	 * @throws None
	 */
	boolean queueMessageHandler(String messageJMS, Map<String, Object> jmsMap) {
		String filter = null;
		String reply;

		log.debug(Helpers.getExternalLogMessage(true, "queueMessageHandler::Message = [" + messageJMS + "]"));
		if (messageJMS == null)
			return false;
		log.info(Helpers.getExternalLogMessage(true, "queueMessageHandler::JMS-MAP = [" + jmsMap + "]"));

		Map<String, Object> req = Helpers.json2map(messageJMS);
		Map<String, Object> jheader = extractedHeader(req);
		Map<String, Object> jdata = extractedData(req);
		log.debug(Helpers.getExternalLogMessage(true, "queueMessageHandler::message=" + req));

		if (req == null || jheader == null)
			return false;
		if (jheader.get(CMD) == null)
			return false;
		if (Arrays.asList(ONE, UPDATE).contains(jheader.get(CMD)) && jdata == null)
			return false;
		if (jheader.get(FILTER) != null)
			filter = (String) jheader.get(FILTER);

		log.debug(Helpers.getInternalLogMessage("Try makeDestination"));
		Map<String, String> replyMap = makeDestination(jmsMap, jheader);
		if (replyMap == null)
			return false;

		log.debug(Helpers.getInternalLogMessage("Try handleCMD"));
		reply = handleCMD(req, (String) jheader.get(CMD), jheader);
		if (reply == null)
			return false;

		log.debug(Helpers.getExternalLogMessage(false,
				"Try to send  replyto=" + replyMap.get(REPLYTO) + "\t queueORtopic=" + replyMap.get(QUEUE_OR_TOPIC)
						+ "\t if=" + (replyMap.get(QUEUE_OR_TOPIC).contains(QUEUE))));
		if (replyMap.get(QUEUE_OR_TOPIC).contains(QUEUE))
			sender.send2queue(reply, replyMap.get(REPLYTO), generateJmsHeqader(jmsMap));
		else
			sender.send2topic(reply, replyMap.get(REPLYTO), filter);

		log.info(Helpers.getInternalLogMessage("handling is OK"));
		return true;
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> extractedData(Map<String, Object> req) {
		if (req == null)
			return null;
		return (Map<String, Object>) req.get("data");
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> extractedHeader(Map<String, Object> req) {
		if (req == null)
			return null;
		return (Map<String, Object>) req.get(HEADER);
	}

	/**
	 * generate reply from heade and data
	 * 
	 * @param header MAP - header of message
	 * @param data   MAP - data of message
	 * @return String - json reply
	 * @throws None
	 */
	private String generateReply(Map<String, Object> header, Map<String, Object> data) {
		String serviceName = serviceSettings.getName();
		Map<String, Object> res = new HashMap<>();
		if (header == null)
			return null;
		header.put(SERVICE_REPLY, serviceName);
		res.put(HEADER, header);
		if (data != null)
			res.put("data", data);
		return Helpers.map2json(res);
	}

	private Map<String, Object> generateJmsHeqader(Map<String, Object> jmsMap) {
		Map<String, Object> jmsHeader = new HashMap<>();
		jmsHeader.put("jmsMessageID", jmsMap.get("jmsMessageID"));
		jmsHeader.put("jmsCorrelationID", jmsMap.get("jmsCorrelationID"));
		jmsHeader.put("jmsPriority", jmsMap.get("jmsPriority"));
		jmsHeader.put("jmsExpiration", jmsMap.get("jmsExpiration"));
		jmsHeader.put("jmsRedelivered", jmsMap.get("jmsRedelivered"));
		return jmsHeader;
	}

	private Map<String, String> makeDestination(Map<String, Object> jmsMap, Map<String, Object> jheader) {
		Map<String, String> ret = new HashMap<>();
		String topicName = jmsSettings.getTopicOutName();
		String queueName = jmsSettings.getQueueOutName();
		String replyto = (String) jmsMap.get("jmsReplyTo");
		String queueORtopic = QUEUE;
		if (replyto != null) {
			Map<String, String> t = Helpers.extractJMSDestination(replyto);
			if (t != null) {
				replyto = t.get(REPLYTO);
				queueORtopic = t.get(QUEUE_OR_TOPIC);
			}
		}
		if (replyto == null) {
			replyto = (String) jheader.get("jmsReplyTo");
		}
		if (replyto == null && (queueName != null && !queueName.equals(""))) {
			replyto = queueName;
		}
		if (replyto == null && (topicName != null && !topicName.equals(""))) {
			replyto = topicName;
			queueORtopic = "topic";
		}
		if (replyto == null)
			return null;

		ret.put(QUEUE_OR_TOPIC, queueORtopic);
		ret.put(REPLYTO, replyto);
		log.info(Helpers.getInternalLogMessage("replyto=" + replyto + "\t queueORtopic=" + queueORtopic
				+ "\t topicName=" + topicName + "\t queueName=" + queueName));
		return ret;
	}

	private String handleCMD(Map<String, Object> req, String cmd, Map<String, Object> jheader) {
		String reply = null;
		Map<String, Object> jdata = extractedData(req);
		Map<String, Object> jsonheader = extractedHeader(req);
		Optional<JPATable> table = null;
		switch (cmd) {
		case VER:
			Map<String, Object> replydata = new HashMap<>();
			replydata.put("version", serviceSettings.getVersion());
			reply = generateReply(jheader, replydata);
			break;
		case NEW:
			table = jpaTableUtil.create(jdata);
			if (log.isDebugEnabled()) {
				log.debug("%n TABLE={}%n", table);
			}
			reply = prepareReply(table, jheader);
			break;
		case UPDATE:
			table = jpaTableUtil.update(jdata);
			reply = prepareReply(table, jheader);
			break;
		case ONE:
			if (jdata != null) {
				table = jpaTableUtil.oneOrNull((String) jdata.get(REFERENCE));
				reply = prepareReply(table, jheader);
			}
			break;
		case FILTER:
			List<JPATable> list = jpaTableUtil.filter(jdata);
			reply = filterOneOrMoreAndSendItBack(list, jdata, jsonheader);
			break;
		case FILTERBYORDERBY:
			List<JPATable> list2 = jpaTableUtil.filterByOrderByLimitTo1(jdata);
			reply = filterOneOrMoreAndSendItBack(list2, jdata, jsonheader);
			break;
		default:
			log.error("Command does not exists!!!");
		}
		log.info(Helpers.getInternalLogMessage("Reply=[" + reply + "]"));
		return reply;
	}

	private String filterOneOrMoreAndSendItBack(List<JPATable> tableList, Map<String, Object> jdata, Map<String, Object> jsonheader) {
		String reply = null;
		try {
			if (jdata != null && jdata.get(LIMIT) != null && jdata.get(LIMIT).equals(1)) {
				reply = prepareReply(tableList.stream().findFirst(), jsonheader);
				if (reply == null) {
					Map<String, Object> data = new HashMap<>();
					data.put("infoMessage", "Data not found.");
					reply = generateReply(jsonheader, data);
				}
			} else {
				reply = complicatedConvert(tableList, jsonheader);
				log.info("FILTER::reply={}", reply);
			}
		} catch (Exception e) {
			log.error("Error during filtering the data", e);
		}
		return reply;
	}

	private String prepareReply(Optional<JPATable> table, Map<String, Object> jheader) {
		if (table.isPresent()) {
			return generateReply(jheader, Helpers.pojo2map(table.get()));
		}
		return null;
	}

	private List<Map<String, Object>> table2map(List<JPATable> items) {
		List<Map<String, Object>> lobj = new LinkedList<>();
		for (JPATable table : items) {
			lobj.add(Helpers.pojo2map(table));
		}
		return lobj;
	}

	private String complicatedConvert(List<JPATable> items, Map<String, Object> jsonheader) {
		List<Map<String, Object>> lobj = table2map(items);
		Map<String, Object> map = new HashMap<>();
		jsonheader.put(SERVICE_REPLY, serviceSettings.getName());
		map.put(HEADER, jsonheader);
		map.put("data", lobj);
		return Helpers.complicated2json(map);
	}
}
