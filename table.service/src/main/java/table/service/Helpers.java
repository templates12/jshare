package table.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.yaml.snakeyaml.Yaml;

import com.fasterxml.jackson.databind.ObjectMapper;
//import com.ibm.jms.JMSBytesMessage;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Helpers {
	private static final String SYSTEM_VARIABLE_URL = "LOGGING_PROPERTIES_URL";
	private static final String SYSTEM_VARIABLE_USER = "LOGGING_PROPERTIES_USER";
	private static final String SYSTEM_VARIABLE_PASSWORD = "LOGGING_PROPERTIES_PASSWORD";

	private Helpers() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Generate a random String with requested lenght
	 *
	 * @param length int - requested lenght
	 * @return String
	 * @throws None
	 */
	public static String generateString(int length) {
		SecureRandom random = new SecureRandom();
		final String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		char[] text = new char[length];

		for (int i = 0; i < length; i++) {
			text[i] = characters.charAt(random.nextInt(characters.length()));
		}
		return new String(text);
	}

	/**
	 * ORM from String json to corresponding Java Object
	 *
	 * @param input json string
	 * @return HashMap java object
	 * @throws IOException
	 */
	public static Map<String, Object> json2map(final String input) {
		JSONTokener parser = new JSONTokener(input);
		JSONObject json = new JSONObject(parser);
		return json.toMap();
	}

	/**
	 * ORM from Java HashMap to json string
	 *
	 * @param obj HashMap
	 * @return java string
	 */
	public static String map2json(final Map<String, Object> obj) {
		JSONObject jobj = new JSONObject(obj);
		return jobj.toString();
	}

	/**
	 * convert from POJO to Map
	 *
	 * @param obj POJO Object
	 * @return Map
	 */
	public static Map<String, Object> pojo2map(final Object obj) {
		ObjectMapper oMapper = new ObjectMapper();
		@SuppressWarnings("unchecked")
		Map<String, Object> reply = oMapper.convertValue(obj, Map.class);
		return reply;
	}

	/**
	 * convert from POJO to json string
	 *
	 * @param obj POJO Object
	 * @return String
	 */
	public static String pojo2json(final Object obj) {
		Map<String, Object> reply = pojo2map(obj);
		return map2json(reply);
	}

	/**
	 * ORM from Java HashMap to json string
	 *
	 * @param obj HashMap
	 * @return java string
	 */
	public static String list2json(final List<Map<String, Object>> lobj) {
		JSONArray jarray = new JSONArray();
		for (Map<String, Object> obj : lobj) {
			JSONObject jobj = new JSONObject(obj);
			jarray.put(jobj);
		}
		if (!jarray.isEmpty()) {
			log.info("list2json={}", jarray);
		}
		return jarray.toString();
	}

	/**
	 * ORM from Java HashMap to json string
	 *
	 * @param obj HashMap
	 * @return java string
	 */
	@SuppressWarnings("unchecked")
	public static String complicated2json(final Map<String, Object> map) {
		JSONObject json = new JSONObject();
		JSONObject jheader = new JSONObject();
		JSONArray jarray = new JSONArray();

		Map<String, Object> heders = (Map<String, Object>) map.get("header");
		for (Map.Entry<String, Object> entry : heders.entrySet()) {
			jheader.put(entry.getKey(), entry.getValue());
		}

		List<Map<String, Object>> data = (List<Map<String, Object>>) map.get("data");
		for (Map<String, Object> obj : data) {
			JSONObject jobj = new JSONObject(obj);
			jarray.put(jobj);
		}

		json.put("header", jheader);
		json.put("data", jarray);

		log.info("json={}", json);

		return json.toString();
	}

	/**
	 * convert from JMS Message to java Map
	 *
	 * @param jmsMessage Message JMS object
	 * @return Map
	 */
	/*
    public static Map<String, Object> jmsIbmMessage2map(JMSBytesMessage jmsMessage) {
		Map<String, Object> reply = null;
		if (jmsMessage == null)
			return null;
		try {

			byte[] byteData = null;
			byteData = new byte[(int) jmsMessage.getBodyLength()];
			jmsMessage.readBytes(byteData);
			jmsMessage.reset();
			String message = new String(byteData);
			String jmsMessageID = jmsMessage.getJMSMessageID();
			int jmsPriority = jmsMessage.getJMSPriority();
			int jmsDeliveryMode = jmsMessage.getJMSDeliveryMode();
			String jmsType = jmsMessage.getJMSType();
			Destination jmsDestination = jmsMessage.getJMSDestination();
			Destination jmsReplyTo = jmsMessage.getJMSReplyTo();
			long jmsExpiration = jmsMessage.getJMSExpiration();
			boolean jmsRedelivered = jmsMessage.getJMSRedelivered();
			long jmsTimestamp = jmsMessage.getJMSTimestamp();

			reply = new HashMap<>();
			reply.put("message", message);
			reply.put("jmsMessageID", jmsMessageID);
			reply.put("jmsCorrelationID", jmsMessageID);
			reply.put("jmsPriority", jmsPriority);
			reply.put("jmsDeliveryMode", jmsDeliveryMode);
			reply.put("jmsType", jmsType);
			if (jmsDestination != null)
				reply.put("jmsDestination", jmsDestination.toString());
			if (jmsReplyTo != null)
				reply.put("jmsReplyTo", jmsReplyTo.toString());
			reply.put("jmsExpiration", jmsExpiration);
			reply.put("jmsRedelivered", jmsRedelivered);
			reply.put("jmsTimestamp", jmsTimestamp);
		} catch (JMSException e) {
			log.error(Helpers.getInternalLogMessage("JMSException " + ErrorCodes.EXCEPTION.toString()), e);
			return null;
		}
		return reply;
	}
    */

	/**
	 * convert from JMS Message to java Map
	 *
	 * @param jmsMessage Message JMS object
	 * @return Map
	 */
	public static Map<String, Object> jmsmessage2map(TextMessage jmsMessage) {
		Map<String, Object> reply = null;
		if (jmsMessage == null)
			return null;
		try {
			String message = jmsMessage.getText();
			String jmsMessageID = jmsMessage.getJMSMessageID();
			int jmsPriority = jmsMessage.getJMSPriority();
			int jmsDeliveryMode = jmsMessage.getJMSDeliveryMode();
			String jmsType = jmsMessage.getJMSType();
			Destination jmsDestination = jmsMessage.getJMSDestination();
			Destination jmsReplyTo = jmsMessage.getJMSReplyTo();
			long jmsExpiration = jmsMessage.getJMSExpiration();
			boolean jmsRedelivered = jmsMessage.getJMSRedelivered();
			long jmsTimestamp = jmsMessage.getJMSTimestamp();

			reply = new HashMap<>();
			reply.put("message", message);
			reply.put("jmsMessageID", jmsMessageID);
			reply.put("jmsCorrelationID", jmsMessageID);
			reply.put("jmsPriority", jmsPriority);
			reply.put("jmsDeliveryMode", jmsDeliveryMode);
			reply.put("jmsType", jmsType);
			if (jmsDestination != null)
				reply.put("jmsDestination", jmsDestination.toString());
			if (jmsReplyTo != null)
				reply.put("jmsReplyTo", jmsReplyTo.toString());
			reply.put("jmsExpiration", jmsExpiration);
			reply.put("jmsRedelivered", jmsRedelivered);
			reply.put("jmsTimestamp", jmsTimestamp);
		} catch (JMSException e) {
			log.error(Helpers.getInternalLogMessage("JMSException " + ErrorCodes.EXCEPTION.toString()), e);
			return null;
		}
		return reply;
	}

	/**
	 * extract destination type and name
	 *
	 * @param String JMS replyTo field
	 * @return Map with replyTo name and queue or topic types
	 */
	public static Map<String, String> extractJMSDestination(String replyTo) {
		// example: queue://ttmtable.outbound.queue
		Map<String, String> ret = null;
		String queueORtopic = null;
		String replyto = null;
		String[] parts = replyTo.split("://");// queue, managername/destination?targetblabla
		String[] queueName = parts[1].split("/");
		if (replyTo.contains("?")) {
			String[] destination = queueName[1].split("\\?");
			replyto = destination[0];
		} else {
			replyto = queueName[0];
		}
		queueORtopic = parts[0];
		ret = new HashMap<>();
		ret.put("queueORtopic", queueORtopic);
		ret.put("replyto", replyto);
		return ret;
	}

	public static List<Object> loadYaml(String name) {
		Yaml yaml = new Yaml();
		List<Object> retvalue = new LinkedList<>();
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		try (InputStream in = loader.getResourceAsStream(name)) {
			Iterable<Object> itr = yaml.loadAll(in);
			for (Object o : itr) {
				retvalue.add(o);
			}
			return retvalue;
		} catch (Exception e) {
			return Collections.emptyList();
		}
	}

	public static Map<String, String> getLogSettings(String yamlFile) {
		Map<String, String> env = System.getenv();
		boolean useSystemVariable = false;
		if (env.containsKey(SYSTEM_VARIABLE_URL) || env.containsKey(SYSTEM_VARIABLE_USER)
				|| env.containsKey(SYSTEM_VARIABLE_PASSWORD)) {
			useSystemVariable = true;
		}

		List<Object> ret = Helpers.loadYaml(yamlFile);
		return readLogSettings(ret, env, useSystemVariable);

	}

	public static Properties readPropertiesClassFile(String resourceName) throws IOException {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		Properties props = null;
		try (InputStream resourceStream = loader.getResourceAsStream(resourceName)) {
			props = new Properties();
			props.load(resourceStream);
		}
		return props;
	}

	public static Properties readPropertiesPath(String filePath) throws IOException {
		final Properties props = new Properties();
		props.load(new FileInputStream(filePath));
		return props;
	}

	public static String getInternalLogMessage(final String message) {
		StringBuilder sb = new StringBuilder();
		sb.append("INTERNAL :");
		sb.append(message);
		return sb.toString();
	}

	public static String getExternalLogMessage(boolean inout, final String message) {
		StringBuilder sb = new StringBuilder();
		if (inout)
			sb.append("EXTERNAL : IN :");
		else
			sb.append("EXTERNAL : OUT :");
		sb.append(message);
		return sb.toString();
	}

	private static Map<String, String> readLogSettings(List<Object> ret, Map<String, String> env,
			boolean useSystemVariable) {
		for (Object o : ret) {
			Map<String, Object> tmp = (Map<String, Object>) o;
			if (tmp.containsKey("logsettings")) {
				return prepareLogSettings(tmp, useSystemVariable, env);
			}
		}
		return Collections.emptyMap();
	}

	private static Map<String, String> prepareLogSettings(Map<String, Object> tmp, boolean useSystemVariable,
			Map<String, String> env) {
		Map<String, String> tmp1 = (Map<String, String>) tmp.get("logsettings");
		if (useSystemVariable) {
			if (env.containsKey(SYSTEM_VARIABLE_URL))
				tmp1.put("url", env.get(SYSTEM_VARIABLE_URL));
			if (env.containsKey(SYSTEM_VARIABLE_USER))
				tmp1.put("user", env.get(SYSTEM_VARIABLE_USER));
			if (env.containsKey(SYSTEM_VARIABLE_PASSWORD))
				tmp1.put("password", env.get(SYSTEM_VARIABLE_PASSWORD));
		}
		return tmp1;
	}

	public static Date convertToDateViaInstant(LocalDateTime dateToConvert) {
		return java.util.Date.from(dateToConvert.atZone(ZoneId.systemDefault()).toInstant());
	}

	public static Date convertStringToDate(String dateToConvert) {
		try {
			return new SimpleDateFormat("yyyy-MM-dd").parse(dateToConvert);
		} catch (ParseException e) {
			log.error(Helpers.getInternalLogMessage("Converting String to date failed with error: " + e));
		}
		return null;
	}
}
