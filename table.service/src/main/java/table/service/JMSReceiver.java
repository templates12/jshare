package table.service;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;


//import com.ibm.jms.JMSBytesMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class JMSReceiver implements MessageListener {
	String latestMsgIn;

	@Autowired	JmsTemplate jmsTemplate;
	@Autowired 	JMSHandler jmsManager;

	/**
	 * get message from destination asynchronously
	 * 
	 * @param destination String - name of queue or topic
	 * @return void
	 * @throws None
	 */
	@JmsListener(destination = "${jmssettings.queueInName}")
	public void onMessage(Message jmsMessage) {
		String msg = null;
		String message = null;
		Map<String, Object> jmsMap = new HashMap<>();
		try {
			log.info("Before message consumption: " + jmsMessage);
			if (jmsMessage instanceof TextMessage) {
				message = ((TextMessage) jmsMessage).getText();
				jmsMap = Helpers.jmsmessage2map((TextMessage) jmsMessage);

			}
		}  catch (JMSException e) {
			e.printStackTrace();
			this.latestMsgIn = null;
		}
		log.info(Helpers.getExternalLogMessage(true, "Message has been consumed : " + message));
		this.latestMsgIn = msg;

		// let's pass original Message because we need JMS meta information
		jmsManager.queueMessageHandler(message, jmsMap);
	}

	/**
	 * get message from destination synchronously
	 * 
	 * @param destination String - name of queue
	 * @return void
	 * @throws None
	 */
	public String receive4destination(String destination) {
		String msg = (String) jmsTemplate.receiveAndConvert(destination);
		log.info(Helpers.getExternalLogMessage(true, "Message has been consumed : " + msg));
		this.latestMsgIn = msg;
		return msg;
	}
}
