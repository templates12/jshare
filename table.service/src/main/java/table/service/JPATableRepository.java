package table.service;

import java.util.Date;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface JPATableRepository extends PagingAndSortingRepository<JPATable, Long> {

	Optional<JPATable> findByReference(String reference);
	Iterable<JPATable> findByStatus(String status);
	Optional<JPATable> findByStatusAndReference(String status, String reference);
	Iterable<JPATable> findByStatusAndCreatedAtDate(String status, Date createdAtDate);
	Iterable<JPATable> findByStatusAndPriority(String status, int priority);
	Iterable<JPATable> findAllByCreatedAtDate(Date createdAtDate);
	//Optional<JPATable> findFirstByStatusOrderByCreatedAtDesc(String status);
	//Optional<JPATable> findFirstByPriorityOrderByCreatedAtDesc(int priority);
	//Optional<JPATable> findFirstByStatusAndPriorityOrderByCreatedAtDesc(String status, int priority);
	Iterable<JPATable> findFirstByStatusOrderByCreatedAtDesc(String status);
	Iterable<JPATable> findFirstByPriorityOrderByCreatedAtDesc(int priority);
	Iterable<JPATable> findFirstByStatusAndPriorityOrderByCreatedAtDesc(String status, int priority);	
}
