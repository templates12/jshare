package table.service;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("jmssettings")
public class ConfigurationJMS {
	String testQueueName;
	String testTopicName;
	String queueInName;
	String queueOutName;
	String topicOutName;
	String topicDefaultFilter;

	String getTestQueueName() {
		return this.testQueueName;
	}

	void setTestQueueName(String x) {
		this.testQueueName = x;
	}

	String getTestTopicName() {
		return this.testTopicName;
	}

	void setTestTopicName(String x) {
		this.testTopicName = x;
	}

	String getQueueInName() {
		return this.queueInName;
	}

	void setQueueInName(String x) {
		this.queueInName = x;
	}

	String getQueueOutName() {
		return this.queueOutName;
	}

	void setQueueOutName(String x) {
		this.queueOutName = x;
	}

	String getTopicOutName() {
		return this.topicOutName;
	}

	void setTopicOutName(String x) {
		this.topicOutName = x;
	}

	String getTopicDefaultFilter() {
		return this.topicDefaultFilter;
	}

	void setTopicDefaultFilter(String x) {
		this.topicDefaultFilter = x;
	}
}
