package table.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(value = "${tableservice.rest.endpoint}")
public class RESTApiController {

	@Autowired
	private JPATableUtils jpaTableUtil;

	@Autowired
	ConfigurationService serviceSettings;

	RESTApiController() {
		jpaTableUtil = new JPATableUtils();
	}

	/**
	 * get version
	 * 
	 * @param None
	 * @return json
	 * @throws None
	 */
	@GetMapping(path = "/ver")
	@ResponseBody
	public Map<String, Object> getVer() {
		log.debug(Helpers.getExternalLogMessage(true, "getVer request received"));
		Map<String, Object> reply = new HashMap<>();
		reply.put("version", serviceSettings.getVersion());
		reply.put("serviceName", serviceSettings.getName());
		log.info(Helpers.getExternalLogMessage(false, "getVer:" + reply.toString()));
		return reply;
	}

	/**
	 * get version
	 * 
	 * @param None
	 * @return json
	 * @throws None
	 */
	@PostMapping(path = "/ver")
	@ResponseBody
	public Map<String, Object> getVerPost() {
		return getVer();
	}

	/**
	 * create one records in DB
	 * 
	 * @param ttmJson - json string
	 * @return Table obj as json
	 * @throws None
	 */
	@PostMapping("/new")
	public Optional<JPATable> setTable() {
		return jpaTableUtil.create(null);
	}

	/**
	 * get all records from DB
	 * 
	 * @param None
	 * @return List
	 * @throws None
	 */
	@PostMapping("/all")
	public Iterable<JPATable> getAll() {
		return jpaTableUtil.all();
	}

	/**
	 * get records from DB by filer
	 * 
	 * @param json filter
	 * @return List
	 * @throws None
	 */
	@PostMapping("/filter")
	public Iterable<JPATable> getByFilter(@RequestBody String dJson) {
		log.info(Helpers.getInternalLogMessage(".............. filter::jsondata ............ =" + dJson));
		Map<String, Object> tMap;
		tMap = Helpers.json2map(dJson);
		log.info(Helpers.getInternalLogMessage(".............. filter::mapdata ............. =" + tMap.toString()));
		return jpaTableUtil.filter(tMap);
	}

	/**
	 * get one records from DB by ttmReference number
	 * 
	 * @param dJson - json string
	 * @return Table obj as json
	 * @throws None
	 */
	@PostMapping("/get")
	public Optional<JPATable> getOne(@RequestBody String dJson) {
		log.info(Helpers.getInternalLogMessage(".............. jsondata ............ =" + dJson));
		Map<String, Object> tMap;
		tMap = Helpers.json2map(dJson);
		log.info(Helpers.getInternalLogMessage(".............. mapdata ............. =" + tMap.toString()));
		return jpaTableUtil.oneOrNull((String) tMap.get("reference"));
	}

	/**
	 * update record by ttmReference number
	 * 
	 * @param ttmJson - json string
	 * @return Table obj as json otherwise Table obj with Error message inside
	 * @throws None
	 */
	@PostMapping("/update")
	public Optional<JPATable> updateTable(@RequestBody String dJson) {
		log.info(Helpers.getInternalLogMessage(".............. jsondata ............ =" + dJson));
		Map<String, Object> tMap;
		tMap = Helpers.json2map(dJson);
		log.info(Helpers.getInternalLogMessage(".............. mapdata ............. =" + tMap.toString()));
		return jpaTableUtil.update(tMap);
	}
}
