package table.service;

public enum ErrorCodes {

    DATANOTFOUND(1, "Data has not been found"),
    DATAISNULL(2, "Data is NULL"),
    PARAMISNULL(3, "Parameter is NULL"),
    UNKNOWNTABLE(4, "Unknown table"),
    RECNOTCREATED(5, "Record has not been created"),
    RECNOTUPDATED(6, "Record has not been updated"),
    RECNOTADDED(7, "Record has not been added"),
    RECNOTDELETED(8, "Record has not been deleted"),
    EXCEPTION(9, "EXCEPTION has occurred, full stack trace follows:");

    private final int errorCode;
    private final String description;

    private ErrorCodes(int errorCode, String description) {
        this.errorCode = errorCode;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public int getErrorCode() {
        return errorCode;
    }

    @Override
    public String toString() {
        return "ErrorCode: " + errorCode + ": " + description;
    }
}
