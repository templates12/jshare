package table.service;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("servicesettings")
public class ConfigurationService {
    String name;
    String version;

    String getName() {return this.name;}
    void setName(String x) {this.name = x;}

    String getVersion() {return this.version;}
    void setVersion(String x) {this.version = x;}
}

