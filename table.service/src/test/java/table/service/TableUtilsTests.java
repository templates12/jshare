package table.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
@SpringBootTest
@ContextConfiguration
class TableUtilsTests {

	@Autowired
	private JPATableRepository testRepository;

	@Autowired
	private JPATableUtils jpaTTMTableUtil = new JPATableUtils();

	@BeforeEach
	public void setUp() throws Exception {
		testRepository.deleteAll();
	}

	@AfterEach
	public void afterMethod() {
		testRepository.deleteAll();
	}

	@Test
	void testCreateEmptyRecord() {
		Optional<JPATable> ttm = jpaTTMTableUtil.create(null);
		Assert.assertTrue(ttm.isPresent());
	}

	@Test
	void testCreateRecord() {
		Map<String, Object> ttmMap = new HashMap<String, Object>();
		ttmMap.put("reference", Helpers.generateString(12));
		ttmMap.put("status", "INIT");
		ttmMap.put("priority", 9999);
		Optional<JPATable> ttm = jpaTTMTableUtil.create(ttmMap);
		Assert.assertTrue(ttm.isPresent());
	}

	@Test
	void testGet() {
		Optional<JPATable> ttm = jpaTTMTableUtil.create(null);
		Optional<JPATable> ttm1 = jpaTTMTableUtil.oneOrNull(ttm.get().reference);
		Assert.assertTrue(ttm.isPresent());
		Assert.assertTrue(ttm1.isPresent());
		assertThat(ttm.get().reference).isEqualTo(ttm1.get().reference);
	}

	@Test
	void testUpdate() throws Exception {
		Optional<JPATable> ttm = jpaTTMTableUtil.create(null);

		Map<String, Object> ttmMap = new HashMap<String, Object>();
		ttmMap.put("reference", ttm.get().reference);
		ttmMap.put("status", "foobarbaz");
		ttmMap.put("priority", 9999);
		Optional<JPATable> ttm2 = jpaTTMTableUtil.update(ttmMap);
		assertEquals("foobarbaz", ttm2.get().status);

		JPATable testTable = testRepository.findByReference((String) ttmMap.get("reference")).get();
		Map<String, Object> testObjectMap = Helpers.pojo2map(testTable);
		for (Map.Entry<String, Object> entry : testObjectMap.entrySet()) {
			for (Map.Entry<String, Object> ttmMapEntry : ttmMap.entrySet()) {
				if (entry.getKey().equals(ttmMapEntry.getKey())) {
					assertEquals(entry.getValue(), ttmMapEntry.getValue());
				}
			}
		}
	}

	@Test
	void testFilter() throws Exception {
		Optional<JPATable> ttm = jpaTTMTableUtil.create(null);
		Map<String, Object> ttmMap = new HashMap<String, Object>();
		ttmMap.put("reference", ttm.get().reference);
		ttmMap.put("priority", 9999);
		ttmMap.put("status", "foobarbaz");
		Optional<JPATable> ttm2 = jpaTTMTableUtil.update(ttmMap);
		assertEquals("foobarbaz", ttm2.get().status);
		Map<String, Object> filMap = new HashMap<String, Object>();
		filMap.put("reference", ttm.get().reference);
		filMap.put("priority", 777);
		Iterable<JPATable> ttm3 = jpaTTMTableUtil.filter(filMap);
		for (JPATable item : ttm3) {
			assertThat(item.priority).isEqualTo(777);
		}
	}
	
	@Test
	void testFilterLimit1() throws Exception {
		Map<String, Object> ttmMap = new HashMap<String, Object>();
		ttmMap.put("reference", Helpers.generateString(10));
		ttmMap.put("status", "foobarbaz");
		jpaTTMTableUtil.create(ttmMap);

		Map<String, Object> ttmMap2 = new HashMap<String, Object>();
		ttmMap.put("reference", Helpers.generateString(10));
		ttmMap.put("status", "foobarbaz");
		jpaTTMTableUtil.create(ttmMap2);

		String ref3 = Helpers.generateString(10);
		Map<String, Object> ttmMap3 = new HashMap<String, Object>();
		ttmMap3.put("reference", ref3);
		ttmMap3.put("status", "foobarbaz");
		jpaTTMTableUtil.create(ttmMap3);

		Map<String, Object> filMap = new HashMap<String, Object>();
		filMap.put("status", "foobarbaz");
		filMap.put("orderby", "createdAt");
		filMap.put("limit", 1);
		
		Iterable<JPATable> res = jpaTTMTableUtil.filterByOrderByLimitTo1(filMap);
		System.out.println("testFilterLimit1 res="+res.toString());
		
		for (JPATable item : res) {
			assertThat(item.reference).isEqualTo(ref3);
		}
	}

}
