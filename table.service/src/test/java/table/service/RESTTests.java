package table.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
//import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = TestConfig.class)
@SpringBootTest
@ContextConfiguration
@AutoConfigureMockMvc
class RESTTests {

	@Value("${tableservice.rest.endpoint}")
	private String ttmUrlEndpoint;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private JPATableRepository ttmtableRepository;

	@Autowired
	private JPATableUtils jpaTTMTableUtil = new JPATableUtils();

	@BeforeEach
	public void setUp() throws Exception {
		ttmtableRepository.deleteAll();
	}

	@Test
	//@WithUserDetails("username")
	void apiTestVer() throws Exception {
		mockMvc.perform(post(ttmUrlEndpoint + "/ver")).andExpect(jsonPath("$.version", is(notNullValue())));
	}

	@Test
	void apiTestVer2() throws Exception {
		mockMvc.perform(get(ttmUrlEndpoint + "/ver")).andExpect(jsonPath("$.version", is(notNullValue())));
	}

	@Test
	//@WithUserDetails("username")
	void apiTestNew() throws Exception {
		mockMvc.perform(post(ttmUrlEndpoint + "/new")).andExpect(jsonPath("$.reference", is(notNullValue())))
				.andExpect(status().isOk());
	}

	@Test
	//@WithUserDetails("username")
	void apiTestGet() throws Exception {
		Optional<JPATable> ttm = jpaTTMTableUtil.create(null);

		Map<String, Object> req = new HashMap<String, Object>();
		req.put("reference", ttm.get().reference);
		String json = Helpers.map2json(req);
		System.out.println("JSON=" + json);

		mockMvc.perform(post(ttmUrlEndpoint + "/get").contentType(MediaType.APPLICATION_JSON).content(json))
				.andExpect(jsonPath("$.reference", is(ttm.get().reference))).andExpect(status().isOk());
	}

	@Test
	//@WithUserDetails("username")
	void apiTestUpdate() throws Exception {
		Optional<JPATable> ttm = jpaTTMTableUtil.create(null);
		String reference = ttm.get().reference;

		Map<String, Object> req = new HashMap<String, Object>();
		req.put("reference", reference);
		req.put("status", "foobarbaz");
		String json = Helpers.map2json(req);
		System.out.println("JSON=" + json);

		mockMvc.perform(post(ttmUrlEndpoint + "/update").contentType(MediaType.APPLICATION_JSON).content(json))
				.andExpect(jsonPath("$.reference", is(reference)))
				.andExpect(jsonPath("$.status", is("foobarbaz"))).andExpect(status().isOk());
	}

	@Test
	//@WithUserDetails("username")
	void apiTestFilter() throws Exception {
		Optional<JPATable> ttm = jpaTTMTableUtil.create(null);
		Optional<JPATable> ttm2 = jpaTTMTableUtil.create(null);
		String reference1 = ttm.get().reference;
		String reference2 = ttm2.get().reference;

		Map<String, Object> req = new HashMap<String, Object>();
		req.put("reference", reference1);
		req.put("type", "SWT");
		req.put("status", "foobarbaz");
		String json = Helpers.map2json(req);
		System.out.println("JSON=" + json);
		mockMvc.perform(post(ttmUrlEndpoint + "/update").contentType(MediaType.APPLICATION_JSON).content(json));

		Map<String, Object> req1 = new HashMap<String, Object>();
		req1.put("reference", reference2);
		req1.put("type", "SWT");
		req1.put("status", "NA");
		String json1 = Helpers.map2json(req1);
		System.out.println("JSON1=" + json1);
		mockMvc.perform(post(ttmUrlEndpoint + "/update").contentType(MediaType.APPLICATION_JSON).content(json1));

		Map<String, Object> req2 = new HashMap<String, Object>();
		req2.put("type", "SWT");
		req2.put("status", "foobarbaz");
		String json2 = Helpers.map2json(req2);
		System.out.println("JSON2=" + json2);

		mockMvc.perform(post(ttmUrlEndpoint + "/filter").contentType(MediaType.APPLICATION_JSON).content(json2))
				.andExpect(status().isOk()).andDo(MockMvcResultHandlers.print())
				.andExpect(jsonPath("$[0].reference", is(reference1)));
	}
}
