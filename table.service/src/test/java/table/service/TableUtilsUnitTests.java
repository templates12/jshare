package table.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
@SpringBootTest
@ContextConfiguration
class TableUtilsUnitTests {
	MockitoSession session;

	@Mock
	private JPATableRepository testRepository;

	@Autowired
	JPATableUtils jpaTTMTableUtil = new JPATableUtils();

	@BeforeEach
	public void setUp() throws Exception {
		testRepository.deleteAll();
		session = Mockito.mockitoSession().initMocks(this).startMocking();
	}

	@AfterEach
	public void afterMethod() {
		session.finishMocking();
	}

	@Test
	void testCreate() {
		JPATableUtils util = Mockito.mock(JPATableUtils.class);
		util.create(null);
		Mockito.verify(util, Mockito.times(1)).create(any());
	}

	@Test
	void testGet() {
		Optional<JPATable> ttm = jpaTTMTableUtil.create(null);
		String reference = ttm.get().reference;
		Optional<JPATable> ttm2 = jpaTTMTableUtil.oneOrNull(reference);
		assertThat(reference).isEqualTo(ttm2.get().reference);
		JPATableUtils util = Mockito.mock(JPATableUtils.class);
		Mockito.when(util.oneOrNull(any())).thenReturn(ttm2);
		util.oneOrNull(reference);
		Mockito.verify(util, Mockito.times(1)).oneOrNull(any());
	}

	@Test
	void testUpdate() throws Exception {
		Optional<JPATable> ttm = jpaTTMTableUtil.create(null);

		Map<String, Object> ttmMap = new HashMap<String, Object>();
		ttmMap.put("reference", ttm.get().reference);
		ttmMap.put("status", "foobarbaz");
		Optional<JPATable> ttm2 = jpaTTMTableUtil.update(ttmMap);
		assertEquals("foobarbaz", ttm2.get().status);

		JPATableUtils util = Mockito.mock(JPATableUtils.class);
		Mockito.when(util.update(any())).thenReturn(ttm2);
		util.update(ttmMap);
		
		Mockito.verify(util).update(any());
	}

	@Test
	void testFilter() throws Exception {
		Optional<JPATable> ttm = jpaTTMTableUtil.create(null);
		Map<String, Object> ttmMap = new HashMap<String, Object>();
		ttmMap.put("reference", ttm.get().reference);
		ttmMap.put("priority", 7);
		ttmMap.put("status", "foobarbaz");
		Optional<JPATable> ttm2 = jpaTTMTableUtil.update(ttmMap);
		assertEquals("foobarbaz", ttm2.get().status);
		Map<String, Object> filMap = new HashMap<String, Object>();
		filMap.put("reference", ttm.get().reference);
		filMap.put("priority", 7);
		List<JPATable> ttm3 = jpaTTMTableUtil.filter(filMap);
		JPATableUtils util = Mockito.mock(JPATableUtils.class);
		Mockito.when(util.filter(any())).thenReturn(ttm3);
		util.filter(filMap);
		Mockito.verify(util).filter(any());
	}
}
