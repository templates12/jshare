package table.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
@SpringBootTest
@ContextConfiguration
class JMSTests {
	@Autowired	ConfigurationService serviceSettings;
	@Autowired	ConfigurationJMS jmsSettings;
	@Autowired	private JPATableRepository ttmtableRepository;
	@Autowired	JmsTemplate jmsTemplate;
	@Autowired	private JMSSender sender;
	@Autowired	private JMSReceiver receiver;
	@Autowired	private JPATableUtils jpaTTMTableUtil = new JPATableUtils();

	boolean replytoShema;

	@BeforeEach
	public void setUp() throws Exception {
		receiver.latestMsgIn = null;
		ttmtableRepository.deleteAll();
	}

	@Test
	@Order(1)
	void testSendReceiveQueue() throws Exception {
		Map<String, Object> header = new HashMap<String, Object>();
		Map<String, Object> req = new HashMap<String, Object>();
		header.put("cmd", "foo");
		header.put("test", "testSendReceiveQueue");
		req.put("header", header);
		String sendMsg = Helpers.map2json(req);

		Map<String, Object> replyheader = new HashMap<String, Object>();
		Map<String, Object> reply = new HashMap<String, Object>();
		replyheader.put("cmd", "foo");
		replyheader.put("test", "testSendReceiveQueue");
		reply.put("header", replyheader);
		String getMsg = Helpers.map2json(reply);

		System.out.println("\n testSendReceiveQueue.sendMsg=[" + sendMsg + "]\n");
		sender.send2queue(sendMsg, jmsSettings.getTestQueueName());
		TimeUnit.SECONDS.sleep(1);
		String msg = receiver.receive4destination(jmsSettings.getTestQueueName());
		System.out.println("\n testSendReceiveQueue.Receive message=[" + msg + "]\n");
		System.out.println("\n testSendReceiveQueue.getMsg=[" + getMsg + "]\n");
		assertThat(msg).isEqualTo(getMsg);
	}

	@Test
	@Order(2)
	void testSendReceiveTopic() throws Exception {
		Map<String, Object> req = new HashMap<String, Object>();
		Map<String, Object> header = new HashMap<String, Object>();
		header.put("cmd", "foo");
		header.put("test", "testSendReceiveTopic");
		req.put("header", header);
		String sendMsg = Helpers.map2json(req);

		Map<String, Object> replyheader = new HashMap<String, Object>();
		Map<String, Object> reply = new HashMap<String, Object>();
		replyheader.put("cmd", "foo");
		replyheader.put("test", "testSendReceiveTopic");
		reply.put("header", replyheader);
		String getMsg = Helpers.map2json(reply);

		System.out.println("\n testSendReceiveTopic.sendMsg=[" + sendMsg + "]\n");
		sender.send2topic(sendMsg, jmsSettings.getTestTopicName(), jmsSettings.getTopicDefaultFilter());
		TimeUnit.SECONDS.sleep(1);
		String msg = receiver.receive4destination(jmsSettings.getTestTopicName());
		System.out.println("\n testSendReceiveTopic.Receive message=[" + msg + "]\n");
		assertThat(msg).isEqualTo(getMsg);
	}

	// @Disabled("Not ready yet")
	@Test
	@Order(3)
	void testJMSManagerVer() throws Exception {
		Map<String, Object> req = new HashMap<String, Object>();
		Map<String, Object> header = new HashMap<String, Object>();
		header.put("cmd", "ver");
		header.put("test", "testJMSManagerVer");
		header.put("jmsReplyTo", jmsSettings.getTestQueueName());
		req.put("header", header);
		String sendMsg = Helpers.map2json(req);
		System.out.println("\n testJMSManagerVer.sendMsg=[" + sendMsg + "]\n");

		Map<String, Object> replyheader = new HashMap<String, Object>();
		Map<String, Object> replydata = new HashMap<String, Object>();
		Map<String, Object> reply = new HashMap<String, Object>();
		replyheader.put("cmd", "ver");
		replyheader.put("test", "testJMSManagerVer");
		replyheader.put("jmsReplyTo", jmsSettings.getTestQueueName());
		replyheader.put("serviceReply", serviceSettings.getName());
		replydata.put("version", "1.0");
		reply.put("header", replyheader);
		reply.put("data", replydata);
		String getMsg = Helpers.map2json(reply);

		sender.send2queue(sendMsg, jmsSettings.getQueueInName());
		TimeUnit.SECONDS.sleep(1);
		String msg = receiver.receive4destination(jmsSettings.getTestQueueName());
		System.out.println("\n testJMSManagerVer.Receive message=[" + msg + "]\n");
		assertThat(msg).isEqualTo(getMsg);
	}

	@Test
	@Order(4)
	void testJMSManagerNew() throws Exception {
		Map<String, Object> req = new HashMap<String, Object>();
		Map<String, Object> header = new HashMap<String, Object>();
		header.put("cmd", "new");
		header.put("test", "testJMSManagerNew");
		header.put("jmsReplyTo", jmsSettings.getTestQueueName());
		req.put("header", header);
		String sendMsg = Helpers.map2json(req);
		System.out.println("\n testJMSManagerNew.sendMsg=[" + sendMsg + "]\n");

		sender.send2queue(sendMsg, jmsSettings.getQueueInName());
		TimeUnit.SECONDS.sleep(1);
		String msg = receiver.receive4destination(jmsSettings.getTestQueueName());

		Map<String, Object> reply = new HashMap<String, Object>();
		reply = Helpers.json2map(msg);
		@SuppressWarnings("unchecked")
		Map<String, Object> replydata = (Map<String, Object>) reply.get("data");
		System.out.println("\n testJMSManagerNew.reply=[" + reply + "]\n");

		assertNotNull(replydata);
		assertNotNull(replydata.get("reference"));
	}

	@Test
	@Order(5)
	void testJMSManagerOne() throws Exception {
		Optional<JPATable> ttm = jpaTTMTableUtil.create(null);

		Map<String, Object> req = new HashMap<String, Object>();
		Map<String, Object> header = new HashMap<String, Object>();
		Map<String, Object> jdata = new HashMap<String, Object>();
		Map<String, Object> jmsheader = new HashMap<String, Object>();
		header.put("cmd", "one");
		header.put("test", "testJMSManagerOne");
		jdata.put("reference", ttm.get().reference);
		req.put("header", header);
		req.put("data", jdata);
		jmsheader.put("jmsReplyTo", jmsSettings.getTestQueueName());
		
		String sendMsg = Helpers.map2json(req);
		System.out.println("\n testJMSManagerOne.sendMsg=[" + sendMsg + "]\n");

		sender.send2queue(sendMsg, jmsSettings.getQueueInName(), jmsheader);
		TimeUnit.SECONDS.sleep(1);
		String msg = receiver.receive4destination(jmsSettings.getTestQueueName());
		System.out.println("\n testJMSManagerOne.Receive message=[" + msg + "]\n");

		Map<String, Object> reply = new HashMap<String, Object>();
		reply = Helpers.json2map(msg);
		System.out.println("\n testJMSManagerOne.reply=[" + reply + "]\n");
		assertNotNull(reply);

		@SuppressWarnings("unchecked")
		Map<String, Object> replydata = (Map<String, Object>) reply.get("data");
		assertNotNull(replydata);
		assertNotNull(replydata.get("reference"));
	}

	@Test
	@Order(6)
	void testJMSManagerUpdate() throws Exception {
		JPATable ttm = new JPATable();
		ttm.reference = Helpers.generateString(12);
		ttm.status = "INIT";
		ttmtableRepository.save(ttm);

		Map<String, Object> req = new HashMap<String, Object>();
		Map<String, Object> header = new HashMap<String, Object>();
		Map<String, Object> jdata = new HashMap<String, Object>();
		header.put("cmd", "update");
		header.put("test", "testJMSManagerUpdate");
		header.put("jmsReplyTo", jmsSettings.getTestQueueName());
		jdata.put("reference", ttm.reference);
		jdata.put("status", "foobarbaz");
		req.put("header", header);
		req.put("data", jdata);
		String sendMsg = Helpers.map2json(req);

		System.out.println("\n testJMSManagerUpdate.sendMsg=[" + sendMsg + "]\n");

		sender.send2queue(sendMsg, jmsSettings.getQueueInName());
		TimeUnit.SECONDS.sleep(1);

		String msg = receiver.receive4destination(jmsSettings.getTestQueueName());
		System.out.println("\n testJMSManagerUpdate.Receive message=[" + msg + "]\n");

		Map<String, Object> reply = new HashMap<String, Object>();
		reply = Helpers.json2map(msg);
		System.out.println("\n testJMSManagerUpdate.reply=[" + reply + "]\n");

		assertNotNull(reply);
		@SuppressWarnings("unchecked")
		Map<String, Object> replydata = (Map<String, Object>) reply.get("data");
		assertNotNull(reply.get("data"));
		assertNotNull(replydata);
		assertNotNull(replydata.get("reference"));
		assertNotNull(replydata.get("status"));
		String replyStatus = (String) replydata.get("status");
		assertThat(replyStatus).isEqualTo("foobarbaz");
	}

	@Test
	@Order(8)
	void testJMSManagerFilterByOrderBy() throws Exception {
		Map<String, Object> jmsheader = new HashMap<>();
		jmsheader.put("jmsDestination", jmsSettings.getQueueInName());
		jmsheader.put("jmsReplyTo", jmsSettings.getQueueOutName());
		jmsheader.put("jmsCorrelationID", Helpers.generateString(10));
		jmsheader.put("jmsPriority", 1);

		Map<String, Object> ttmMap = new HashMap<String, Object>();
		ttmMap.put("reference", Helpers.generateString(10));
		ttmMap.put("priority", 1);
		ttmMap.put("status", "foobarbaz");
		jpaTTMTableUtil.create(ttmMap);

		Map<String, Object> ttmMap2 = new HashMap<String, Object>();
		ttmMap2.put("reference", Helpers.generateString(10));
		ttmMap2.put("priority", 1);
		ttmMap2.put("status", "foobarbaz");
		jpaTTMTableUtil.create(ttmMap2);

		String ref3 = Helpers.generateString(10);
		Map<String, Object> ttmMap3 = new HashMap<String, Object>();
		ttmMap3.put("reference", ref3);
		ttmMap3.put("priority", 2);
		ttmMap3.put("status", "foobarbaz");
		jpaTTMTableUtil.create(ttmMap3);

		Map<String, Object> req = new HashMap<String, Object>();
		Map<String, Object> jheader = new HashMap<String, Object>();
		Map<String, Object> jdata = new HashMap<String, Object>();
		jheader.put("cmd", "filterbyorderby");
		jheader.put("test", "testJMSManagerFilterByOrderBy");
		jdata.put("priority", 2);
		jdata.put("status", "foobarbaz");
		jdata.put("orderby", "createdAt");
		jdata.put("limit", 1);
		req.put("header", jheader);
		req.put("data", jdata);
		String sendMsg = Helpers.map2json(req);

		System.out.println("testJMSManagerFilterByOrderBy.sendMsg=[" + sendMsg + "]\n");

		sender.send2queue(sendMsg, jmsSettings.getQueueInName(), jmsheader);
		TimeUnit.SECONDS.sleep(1);

		String msg = receiver.receive4destination(jmsSettings.getQueueOutName());
		System.out.println("testJMSManagerFilterByOrderBy.Receive message=[" + msg + "]\n");

		Map<String, Object> reply = new HashMap<String, Object>();
		reply = Helpers.json2map(msg);
		System.out.println("testJMSManagerFilterByOrderBy.reply=" + reply + "\n");
		assertNotNull(reply);

		@SuppressWarnings("unchecked")
		Map<String, Object> replydata = (Map<String, Object>)reply.get("data");
		assertNotNull(reply.get("data"));
		assertNotNull(replydata);
		System.out.println("testJMSManagerFilterByOrderBy.replydata=" + replydata.toString() + "\n");

		assertNotNull(replydata.get("reference"));
		assertNotNull(replydata.get("priority"));
		assertThat((String)replydata.get("reference")).contains(ref3);
		assertThat((int)replydata.get("priority")).isEqualTo(2);
	}
}
