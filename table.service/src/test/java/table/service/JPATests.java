package table.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
//import org.junit.Ignore;
import org.junit.runner.RunWith;
//import static org.mockito.Mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
@SpringBootTest
@ContextConfiguration
class JPATests {
	@Autowired	private JPATableRepository tableRepository;

	@BeforeEach
	public void setUp() throws Exception {
		tableRepository.deleteAll();
	}

	@Test
	void ttmtableTest() {
		JPATable tbl = new JPATable();
		tbl.reference = Helpers.generateString(12);
		tableRepository.save(tbl);

		Optional<JPATable> persistedres = tableRepository.findById(tbl.getId());
		System.out.println(persistedres.toString());
		System.out.println("Exists=" + persistedres.isPresent());
		System.out.println("createAt=" + persistedres.get().createdAt);

		Assert.assertTrue(persistedres.isPresent());
		Assert.assertNotNull(persistedres.get().reference);
	}

}