package table.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
@SpringBootTest
@ContextConfiguration
class HelpersTests {

	@Test
	void extractJMSDestinationTest() {
		String t = "queue://manager/ttmtable.outbound.queue?target=1";
		Map<String, String> ret = Helpers.extractJMSDestination(t);
		System.out.println(ret.toString());
		assertThat(ret.get("queueORtopic")).contains("queue");
		assertThat(ret.get("replyto")).contains("ttmtable.outbound.queue");

	}

	@Test
	void loadYamlTest() {
		Map<String, String> ret = Helpers.getLogSettings("application.yml");
		assertNotNull(ret);
		System.out.println("application.yml=" + ret);
		Map<String, String> env = System.getenv();
		System.out.println("env=" + env);
	}
}