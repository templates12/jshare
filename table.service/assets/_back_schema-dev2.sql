-- It is intended for PostgreSQL databases.




CREATE SEQUENCE IF NOT EXISTS LOGGING_EVENT_ID_SEQ MINVALUE 1 START 1;


CREATE TABLE IF NOT EXISTS TTM_TABLE_LOGGING_EVENT
  (
    timestmp         BIGINT NOT NULL,
    formatted_message  TEXT NOT NULL,
    logger_name       VARCHAR(254) NOT NULL,
    level_string      VARCHAR(254) NOT NULL,
    thread_name       VARCHAR(254),
    reference_flag    SMALLINT,
    arg0              VARCHAR(254),
    arg1              VARCHAR(254),
    arg2              VARCHAR(254),
    arg3              VARCHAR(254),
    caller_filename   VARCHAR(254) NOT NULL,
    caller_class      VARCHAR(254) NOT NULL,
    caller_method     VARCHAR(254) NOT NULL,
    caller_line       CHAR(4) NOT NULL,
    event_id          BIGINT DEFAULT nextval('LOGGING_EVENT_ID_SEQ') PRIMARY KEY
  );

CREATE TABLE IF NOT EXISTS TTM_TABLE_LOGGING_EVENT_PROPERTY
  (
    event_id	      BIGINT NOT NULL,
    mapped_key        VARCHAR(254) NOT NULL,
    mapped_value      VARCHAR(1024),
    PRIMARY KEY(event_id, mapped_key),
    FOREIGN KEY (event_id) REFERENCES TTM_TABLE_LOGGING_EVENT(event_id)
  );

CREATE TABLE IF NOT EXISTS TTM_TABLE_LOGGING_EVENT_EXCEPTION
  (
    event_id         BIGINT NOT NULL,
    i                SMALLINT NOT NULL,
    trace_line       TEXT NOT NULL,
    PRIMARY KEY(event_id, i),
    FOREIGN KEY (event_id) REFERENCES TTM_TABLE_LOGGING_EVENT(event_id)
  );