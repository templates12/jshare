--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2
-- Dumped by pg_dump version 12.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: application_log_sequence; Type: SEQUENCE; Schema: public; Owner: ta2ctis
--

CREATE SEQUENCE public.application_log_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.application_log_sequence OWNER TO ta2ctis;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: guitable; Type: TABLE; Schema: public; Owner: ttmuser
--

CREATE TABLE public.guitable (
    id bigint NOT NULL,
    create_at timestamp without time zone,
    last_modified timestamp without time zone,
    branch_entity character varying(255),
    branch_id integer NOT NULL,
    business_reference character varying(255),
    customer_id integer NOT NULL,
    done_date timestamp without time zone,
    external_id character varying(255),
    initial_contract_dataset character varying(255),
    inizational_system character varying(255),
    mbe character varying(255),
    other_system character varying(255),
    pe_instance_id character varying(255),
    product character varying(255),
    receiverbic character varying(255),
    related_ttm_ref character varying(255),
    senderbic character varying(255),
    start_date character varying(255),
    status character varying(255),
    style character varying(255),
    swift character varying(255),
    swift_hash character varying(255),
    system_id character varying(255),
    tag20 character varying(255),
    tag21 character varying(255),
    tn_reference character varying(255),
    trafinas_branch_id character varying(255),
    trafinas_case_no character varying(255),
    trafinas_check_status character varying(255),
    trafinas_event_id character varying(255),
    trafinas_get_res_at timestamp without time zone,
    trafinas_relevance character varying(255),
    trafinas_send_req_at timestamp without time zone,
    trafinas_time_stamp character varying(255),
    ttm_reference character varying(255),
    type character varying(255),
    create_at date not null default current_date
);


ALTER TABLE public.guitable OWNER TO ttmuser;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: tauser
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO tauser;

--
-- Name: jpabranch_table; Type: TABLE; Schema: public; Owner: ttmuser
--

CREATE TABLE public.jpabranch_table (
    id bigint NOT NULL,
    branch_id integer NOT NULL,
    branchentity character varying(255),
    product character varying(255)
);


ALTER TABLE public.jpabranch_table OWNER TO ttmuser;

--
-- Name: jpacust_excp_table; Type: TABLE; Schema: public; Owner: ttmuser
--

CREATE TABLE public.jpacust_excp_table (
    id bigint NOT NULL,
    branchentity character varying(255),
    customer_id integer NOT NULL,
    product character varying(255)
);


ALTER TABLE public.jpacust_excp_table OWNER TO ttmuser;

--
-- Name: jpainit_data_set_table; Type: TABLE; Schema: public; Owner: ttmuser
--

CREATE TABLE public.jpainit_data_set_table (
    id bigint NOT NULL,
    style character varying(255),
    type character varying(255),
    initial_contract_dataset character varying(255)
);


ALTER TABLE public.jpainit_data_set_table OWNER TO ttmuser;

--
-- Name: jpaproduct_table; Type: TABLE; Schema: public; Owner: ttmuser
--

CREATE TABLE public.jpaproduct_table (
    id bigint NOT NULL,
    style character varying(255),
    type character varying(255),
    product character varying(255)
);


ALTER TABLE public.jpaproduct_table OWNER TO ttmuser;

--
-- Name: jpasystem_table; Type: TABLE; Schema: public; Owner: ttmuser
--

CREATE TABLE public.jpasystem_table (
    id bigint NOT NULL,
    branchentity character varying(255),
    system_id character varying(255)
);


ALTER TABLE public.jpasystem_table OWNER TO ttmuser;

--
-- Name: jpatable; Type: TABLE; Schema: public; Owner: ta2ctis
--

CREATE TABLE public.jpatable (
    id bigint NOT NULL,
    create_at timestamp without time zone,
    last_modified timestamp without time zone,
    branch_entity character varying(255),
    branch_id integer NOT NULL,
    business_reference character varying(255),
    customer_id integer NOT NULL,
    done_date timestamp without time zone,
    external_id character varying(255),
    initial_contract_dataset character varying(255),
    inizational_system character varying(255),
    mbe character varying(255),
    other_system character varying(255),
    pe_instance_id character varying(255),
    product character varying(255),
    receiverbic character varying(255),
    related_ttm_ref character varying(255),
    senderbic character varying(255),
    start_date character varying(255),
    status character varying(255),
    style character varying(255),
    swift character varying(255),
    swift_hash character varying(255),
    system_id character varying(255),
    tag20 character varying(255),
    tag21 character varying(255),
    tn_reference character varying(255),
    trafinas_branch_id character varying(255),
    trafinas_case_no character varying(255),
    trafinas_check_status character varying(255),
    trafinas_event_id character varying(255),
    trafinas_get_res_at timestamp without time zone,
    trafinas_relevance character varying(255),
    trafinas_send_req_at timestamp without time zone,
    trafinas_time_stamp character varying(255),
    ttm_reference character varying(255),
    type character varying(255)
);


ALTER TABLE public.jpatable OWNER TO ta2ctis;

--
-- Name: jpatrafinas_case_eventtype_table; Type: TABLE; Schema: public; Owner: ttmuser
--

CREATE TABLE public.jpatrafinas_case_eventtype_table (
    id bigint NOT NULL,
    style character varying(255),
    type character varying(255),
    product character varying(255),
    trafinascasetype character varying(255),
    trafinaseventtype character varying(255)
);


ALTER TABLE public.jpatrafinas_case_eventtype_table OWNER TO ttmuser;

--
-- Name: jpatrafinas_rel_table; Type: TABLE; Schema: public; Owner: ttmuser
--

CREATE TABLE public.jpatrafinas_rel_table (
    id bigint NOT NULL,
    style character varying(255),
    type character varying(255),
    tn_relevance character varying(255)
);


ALTER TABLE public.jpatrafinas_rel_table OWNER TO ttmuser;

--
-- Name: jpauser_app_table; Type: TABLE; Schema: public; Owner: ttmuser
--

CREATE TABLE public.jpauser_app_table (
    id bigint NOT NULL,
    application character varying(255),
    osystem character varying(255),
    use character varying(255)
);


ALTER TABLE public.jpauser_app_table OWNER TO ttmuser;

--
-- Data for Name: guitable; Type: TABLE DATA; Schema: public; Owner: ttmuser
--

COPY public.guitable (id, create_at, last_modified, branch_entity, branch_id, business_reference, customer_id, done_date, external_id, initial_contract_dataset, inizational_system, mbe, other_system, pe_instance_id, product, receiverbic, related_ttm_ref, senderbic, start_date, status, style, swift, swift_hash, system_id, tag20, tag21, tn_reference, trafinas_branch_id, trafinas_case_no, trafinas_check_status, trafinas_event_id, trafinas_get_res_at, trafinas_relevance, trafinas_send_req_at, trafinas_time_stamp, ttm_reference, type) FROM stdin;
\.


--
-- Data for Name: jpabranch_table; Type: TABLE DATA; Schema: public; Owner: ttmuser
--

COPY public.jpabranch_table (id, branch_id, branchentity, product) FROM stdin;
\.


--
-- Data for Name: jpacust_excp_table; Type: TABLE DATA; Schema: public; Owner: ttmuser
--

COPY public.jpacust_excp_table (id, branchentity, customer_id, product) FROM stdin;
\.


--
-- Data for Name: jpainit_data_set_table; Type: TABLE DATA; Schema: public; Owner: ttmuser
--

COPY public.jpainit_data_set_table (id, style, type, initial_contract_dataset) FROM stdin;
86	MT405	SWT	Y
87	MT760	SWT	Y
88	MT700	SWT	Y
89	MT705	SWT	Y
90	MT710	SWT	Y
91	MT720	SWT	Y
92	MT740	SWT	Y
\.


--
-- Data for Name: jpaproduct_table; Type: TABLE DATA; Schema: public; Owner: ttmuser
--

COPY public.jpaproduct_table (id, style, type, product) FROM stdin;
20	MT405	SWT	IC
21	MT760	SWT	GI
22	MT700	SWT	EL
23	MT701	SWT	EL
24	MT705	SWT	EL
25	MT710	SWT	EL
26	MT711	SWT	EL
27	MT720	SWT	EL
28	MT721	SWT	EL
29	MT740	SWT	RM
30	MT742	SWT	RM
\.


--
-- Data for Name: jpasystem_table; Type: TABLE DATA; Schema: public; Owner: ttmuser
--

COPY public.jpasystem_table (id, branchentity, system_id) FROM stdin;
103	SG1	TI+ Zone 1
104	SG2	TI+ Zone 1
105	TOK	TI+ Zone 1
106	HON	TI+ Zone 1
107	SHA	TI+ Zone 1
108	BJG	TI+ Zone 1
109	MOS	TI+ Zone 3
110	AMS	TI+ Zone 2
111	MIL	TI+ Zone 2
112	PAR	TI+ Zone 2
113	MAD	TI+ Zone 2
114	BCN	TI+ Zone 2
115	BRU	TI+ Zone 2
116	BRA	TI+ Zone 2
117	VNA	TI+ Zone 2
118	BUD	TI+ Zone 2
119	ZUE	TI+ Zone 2
120	PRA	TI+ Zone 2
\.


--
-- Data for Name: jpatable; Type: TABLE DATA; Schema: public; Owner: ta2ctis
--

COPY public.jpatable (id, create_at, last_modified, branch_entity, branch_id, business_reference, customer_id, done_date, external_id, initial_contract_dataset, inizational_system, mbe, other_system, pe_instance_id, product, receiverbic, related_ttm_ref, senderbic, start_date, status, style, swift, swift_hash, system_id, tag20, tag21, tn_reference, trafinas_branch_id, trafinas_case_no, trafinas_check_status, trafinas_event_id, trafinas_get_res_at, trafinas_relevance, trafinas_send_req_at, trafinas_time_stamp, ttm_reference, type) FROM stdin;
18	2020-05-19 13:30:17.259	\N		-1		-1	\N																								\N		\N		oLC1L2YoVI8O	
19	2020-05-19 13:42:20.472	\N		-1		-1	\N																								\N		\N		rZbGxKGxicEg	
121	2020-05-19 16:53:35.635	\N		-1		-1	\N		Y	NTDB_Default			dfe87937-88ba-4994-9ef5-bb09fadca9ba		COBAXXFFEMB		COBADEFFCPL			MT700	{1:F01COBAXXFFAEMB0000000000}{2:O7EM1137200422COBADEFFACPL00000000002004221137N}{4:					FirstTest12457666					\N		\N		2Q5AM8BPeAbN	SWT
122	2020-05-20 13:10:45.506	\N		-1		-1	\N		Y	NTDB_Default			b418474e-e56b-42e0-bc15-fc5202d9fdd5		COBAXXFFEMB		COBADEFFCPL			MT700	{1:F01COBADEFFDXXX7945936481}{2:O7300416200519BKAUATW0BXXX80354340642005190416N}{4:					FirstTest12457666					\N		\N		ddKKkeyPkasy	SWT
123	2020-05-20 17:23:42.762	\N		-1		-1	\N																								\N		\N		RBB5JV86myQf	
124	2020-05-21 10:25:00.599	\N		-1		-1	\N																								\N		\N		DVDHf0aGmXJn	
\.


--
-- Data for Name: jpatrafinas_case_eventtype_table; Type: TABLE DATA; Schema: public; Owner: ttmuser
--

COPY public.jpatrafinas_case_eventtype_table (id, style, type, product, trafinascasetype, trafinaseventtype) FROM stdin;
1	MT700	SWT	EL	ELC	REC
2	MT707	SWT	EL	ELC	AME
\.


--
-- Data for Name: jpatrafinas_rel_table; Type: TABLE DATA; Schema: public; Owner: ttmuser
--

COPY public.jpatrafinas_rel_table (id, style, type, tn_relevance) FROM stdin;
31	MT700	SWT	Y
32	MT701	SWT	Y
33	MT705	SWT	Y
34	MT707	SWT	Y
35	MT708	SWT	Y
36	MT710	SWT	Y
37	MT711	SWT	Y
38	MT720	SWT	Y
39	MT721	SWT	Y
40	MT730	SWT	Y
41	MT732	SWT	Y
42	MT734	SWT	Y
43	MT740	SWT	Y
44	MT742	SWT	Y
45	MT744	SWT	Y
46	MT747	SWT	Y
47	MT750	SWT	Y
48	MT752	SWT	Y
49	MT754	SWT	Y
50	MT756	SWT	Y
51	MT759	SWT	Y
52	MT760	SWT	Y
53	MT761	SWT	Y
54	MT765	SWT	Y
55	MT767	SWT	Y
56	MT768	SWT	Y
57	MT769	SWT	Y
58	MT775	SWT	Y
59	MT785	SWT	Y
60	MT752	SWT	Y
61	MT787	SWT	Y
62	MT790	SWT	Y
63	MT791	SWT	Y
64	MT792	SWT	Y
65	MT795	SWT	Y
66	MT796	SWT	Y
67	MT798	SWT	Y
68	MT799	SWT	Y
69	MT400	SWT	Y
70	MT410	SWT	Y
71	MT412	SWT	Y
72	MT416	SWT	Y
73	MT420	SWT	Y
74	MT422	SWT	Y
75	MT430	SWT	Y
76	MT450	SWT	Y
77	MT455	SWT	Y
78	MT456	SWT	Y
79	MT490	SWT	Y
80	MT491	SWT	Y
81	MT492	SWT	Y
82	MT495	SWT	Y
83	MT496	SWT	Y
84	MT498	SWT	Y
85	MT499	SWT	Y
\.


--
-- Data for Name: jpauser_app_table; Type: TABLE DATA; Schema: public; Owner: ttmuser
--

COPY public.jpauser_app_table (id, application, osystem, use) FROM stdin;
93	Trafinas		N
94	Doka5INL	CoDOKAHOST	N
95	Doka5LON	CoDOKAHOST	N
96	Doka5LTSB	CoDOKAHOST	N
97	TIZ1	TI	N
98	TIZ2	TI	N
99	TIZ3	TI	N
100	IBSPHXAsia	PHX	N
101	IBSPHXEuro	PHX	N
102	GTSNY	GTSNY	N
\.


--
-- Name: application_log_sequence; Type: SEQUENCE SET; Schema: public; Owner: ta2ctis
--

SELECT pg_catalog.setval('public.application_log_sequence', 1, false);


--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: tauser
--

SELECT pg_catalog.setval('public.hibernate_sequence', 124, true);


--
-- Name: guitable guitable_pkey; Type: CONSTRAINT; Schema: public; Owner: ttmuser
--

ALTER TABLE ONLY public.guitable
    ADD CONSTRAINT guitable_pkey PRIMARY KEY (id);


--
-- Name: jpabranch_table jpabranch_table_pkey; Type: CONSTRAINT; Schema: public; Owner: ttmuser
--

ALTER TABLE ONLY public.jpabranch_table
    ADD CONSTRAINT jpabranch_table_pkey PRIMARY KEY (id);


--
-- Name: jpacust_excp_table jpacust_excp_table_pkey; Type: CONSTRAINT; Schema: public; Owner: ttmuser
--

ALTER TABLE ONLY public.jpacust_excp_table
    ADD CONSTRAINT jpacust_excp_table_pkey PRIMARY KEY (id);


--
-- Name: jpainit_data_set_table jpainit_data_set_table_pkey; Type: CONSTRAINT; Schema: public; Owner: ttmuser
--

ALTER TABLE ONLY public.jpainit_data_set_table
    ADD CONSTRAINT jpainit_data_set_table_pkey PRIMARY KEY (id);


--
-- Name: jpaproduct_table jpaproduct_table_pkey; Type: CONSTRAINT; Schema: public; Owner: ttmuser
--

ALTER TABLE ONLY public.jpaproduct_table
    ADD CONSTRAINT jpaproduct_table_pkey PRIMARY KEY (id);


--
-- Name: jpasystem_table jpasystem_table_pkey; Type: CONSTRAINT; Schema: public; Owner: ttmuser
--

ALTER TABLE ONLY public.jpasystem_table
    ADD CONSTRAINT jpasystem_table_pkey PRIMARY KEY (id);


--
-- Name: jpatable jpatable_pkey; Type: CONSTRAINT; Schema: public; Owner: ta2ctis
--

ALTER TABLE ONLY public.jpatable
    ADD CONSTRAINT jpatable_pkey PRIMARY KEY (id);


--
-- Name: jpatrafinas_case_eventtype_table jpatrafinas_case_eventtype_table_pkey; Type: CONSTRAINT; Schema: public; Owner: ttmuser
--

ALTER TABLE ONLY public.jpatrafinas_case_eventtype_table
    ADD CONSTRAINT jpatrafinas_case_eventtype_table_pkey PRIMARY KEY (id);


--
-- Name: jpatrafinas_rel_table jpatrafinas_rel_table_pkey; Type: CONSTRAINT; Schema: public; Owner: ttmuser
--

ALTER TABLE ONLY public.jpatrafinas_rel_table
    ADD CONSTRAINT jpatrafinas_rel_table_pkey PRIMARY KEY (id);


--
-- Name: jpauser_app_table jpauser_app_table_pkey; Type: CONSTRAINT; Schema: public; Owner: ttmuser
--

ALTER TABLE ONLY public.jpauser_app_table
    ADD CONSTRAINT jpauser_app_table_pkey PRIMARY KEY (id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: ta2ctis
--

GRANT USAGE ON SCHEMA public TO tauser;
GRANT USAGE ON SCHEMA public TO ttmuser;


--
-- Name: SEQUENCE application_log_sequence; Type: ACL; Schema: public; Owner: ta2ctis
--

GRANT ALL ON SEQUENCE public.application_log_sequence TO ttmuser;


--
-- Name: SEQUENCE hibernate_sequence; Type: ACL; Schema: public; Owner: tauser
--

GRANT ALL ON SEQUENCE public.hibernate_sequence TO ttmuser;


--
-- Name: TABLE jpatable; Type: ACL; Schema: public; Owner: ta2ctis
--

GRANT ALL ON TABLE public.jpatable TO ttmuser;
GRANT SELECT ON TABLE public.jpatable TO tauser;


--
-- PostgreSQL database dump complete
--

