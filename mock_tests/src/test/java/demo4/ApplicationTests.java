package demo4;


import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
//@ContextConfiguration(classes = Application.class)
@RestClientTest(ClientService.class)
//@AutoConfigureWebClient(registerRestTemplate = true)
class ApplicationTests{
    MockitoSession session;

    @Autowired
    ResourceLoader resourceLoader;
    
    @Autowired
    private ClientService clientService;
    
    @Autowired
    private MockRestServiceServer server;  // for mocking of real external server

	@Before
	public void setUp() throws Exception {
        session = Mockito.mockitoSession().initMocks(this).startMocking();
        System.out.println("@Before will be invoked before each test");
	}

    @After
    public void afterMethod() {
        session.finishMocking();
        System.out.println("@After will be invoked after each test");        
    }


    @Test 
    public void testMockGetVersion() {   	
    	Resource resource = resourceLoader.getResource("classpath:version.json");
    	this.server.expect(requestTo("http://localhost:8080/mockserver/ver")).
    				andRespond(withSuccess(resource, MediaType.APPLICATION_JSON));
    	
    	String val = clientService.getVersion();
    	assertThat(val).isEqualTo("{\"version\":\"1.0\"}");
    }

    @Test 
    public void testMockGetCount() {   	
    	Resource resource = resourceLoader.getResource("classpath:count.json");
    	this.server.expect(requestTo("http://localhost:8080/mockserver/count")).
    				andRespond(withSuccess(resource, MediaType.APPLICATION_JSON));
    	
    	String val = clientService.getCount();
    	assertThat(val).isEqualTo("{\"one\":1,\"two\":2,\"three\":3}");
    }
}
