package demo4;


import org.springframework.stereotype.Component;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Component
@ConfigurationProperties("app")
public class ConfigurationApplication {
  int serverport;
  String testQueueName;
  String testTopicName;
  String topicDefaultFilter;
  String topicOutName;
  String queueInName;
  String queueOutName;
  
  int getServerport() {return this.serverport;}
  void setServerport(int x) {this.serverport = x;}
  
  String getTestQueueName() {return this.testQueueName;}
  void setTestQueueName(String x) {this.testQueueName = x;}

  String getTestTopicName() {return this.testTopicName;}
  void setTestTopicName(String x) {this.testTopicName = x;}

  String getTopicDefaultFilter() {return this.topicDefaultFilter;}
  void setTopicDefaultFilter(String x) {this.topicDefaultFilter = x;}

  String getTopicOutName() {return this.topicOutName;}
  void setTopicOutName(String x) {this.topicOutName = x;}

  String getQueueInName() {return this.queueInName;}
  void setQueueInName(String x) {this.queueInName = x;}

  String getQueueOutName() {return this.queueOutName;}
  void setQueueOutName(String x) {this.queueOutName = x;}
}