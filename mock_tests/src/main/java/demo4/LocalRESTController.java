package demo4;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;
import java.util.Optional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/mockserver")
public class LocalRESTController {
	private final Logger log = LoggerFactory.getLogger(getClass());
	
    /**
     * get version
     * @param  None
     * @return json
     * @throws None
     */   
    @GetMapping("/ver")
    @ResponseBody
    public Map<String, Object> getVer() {
        Map<String, Object> reply = new HashMap<String, Object>();
        reply.put("version", "1.0");
        log.debug("getVer:" + reply.toString());
        return reply;
    }
    
    /**
     * create one records in DB
     * @param  ttmJson - json string
     * @return TTMTable obj as json
     * @throws None
     */
    @GetMapping("/count")
    public Optional<Map> getCount() {
        Map<String, Object> reply = new HashMap<String, Object>();
        reply.put("one", 1);
        reply.put("two", 2);
        reply.put("three", 3);
        log.debug("getList:" + reply.toString());
    	return Optional.ofNullable(reply);
    }
}
