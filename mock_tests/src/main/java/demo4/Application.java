package demo4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@ComponentScan
@EnableConfigurationProperties
public class Application {
	static ConfigurationApplication appSettings;
	static ConfigurableApplicationContext context;
	//static LocalRESTController restServer;
	
	public static void main(String[] args) {
		context = SpringApplication.run(Application.class, args);
		appSettings = context.getBean(ConfigurationApplication.class);
		//restServer = context.getBean(LocalRESTController.class);
		
		System.out.println("Confihuration = " + appSettings.toString());
	}
}