package demo4;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class ClientService {
	private final Logger log = LoggerFactory.getLogger(getClass());
    RestTemplate restTemplate;
    
    @Autowired
    public ClientService(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.build();
    }
	
	public String getVersion() {
		log.info("getVersion");
		ResponseEntity<String> response  = restTemplate.getForEntity("http://localhost:8080/mockserver/ver", String.class);
		log.info("response="+response);
		if (response.getStatusCode() != HttpStatus.OK)
			return null;
		return response.getBody();
		//assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
	}
	
	public String getCount() {
		log.info("getCount");
		ResponseEntity<String> response  = restTemplate.getForEntity("http://localhost:8080/mockserver/count", String.class);
		log.info("response="+response);
		if (response.getStatusCode() != HttpStatus.OK)
			return null;
		return response.getBody();
	}
}
