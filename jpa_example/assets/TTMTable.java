package com.example.demo;

/**
author: Kirill.Rozin@gmail.com
**/

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class TTMTable {

   @Id
   @GeneratedValue
   public Long id;
	public String status;
	public String ttmReference;
	public String inizationalSystem;
	public String relatedTtmRef;
	public String businessReference;
	public String externalId;
	public String senderBic;
	public String receiverBic;
	public String type;
	public String product;
	public String branchLocation;
	public String mbe;
	public String systemId;
	public String tag20;
	public String tag21;
	public String trafinasBranchId;
	public String trafinasCaseId;
	public String trafinasEventId;
	public String otherSystem;
	public int style;
	public int customerId;
	public int branchId;
	public LocalDateTime createdAt;
	public LocalDateTime doneAt;
	public LocalDateTime trafinasSendReqAt;
	public LocalDateTime trafinasGetResAt;
}
