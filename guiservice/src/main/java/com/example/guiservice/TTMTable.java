package com.example.guiservice;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TTMTable {

    private Long id;

    private String status;
    private String ttmReference;
    private String inizationalSystem;
    private String relatedTtmRef;
    private String businessReference;
    private String externalId;
    private String senderBic;
    private String receiverBic;
    private String type;
    private String product;
    private String branchLocation;
    private String mbe;
    private String systemId;
    private String tag20;
    private String tag21;
    private String trafinasBranchId;
    private String trafinasCaseId;
    private String trafinasEventId;
    private String otherSystem;
    private int style;
    private int customerId;
    private int branchId;
    private LocalDateTime createdAt;
    private LocalDateTime doneAt;
    private LocalDateTime trafinasSendReqAt;
    private LocalDateTime trafinasGetResAt;
}