package com.example.guiservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.json.*;
import org.json.JSONTokener;

@Service
public class JMSTTMTableService {

    private static final Logger logger = Logger.getLogger(JMSTTMTableService.class.getName());

    @Autowired
    private JMSSender sender;

    @Autowired
    private JMSReceiver receiver;

    public TTMTable  map2table(  Map<String, Object> ttmMap) {
        TTMTable ttm = new TTMTable();
        if (ttmMap.get("ttmReference") == null) return  null;
        else {
            ttm.setTtmReference((String) ttmMap.get("ttmReference"));
        }
        if (ttmMap.get("status") != null)
            ttm.setStatus((String) ttmMap.get("status"));
        if (ttmMap.get("inizationalSystem") != null)
            ttm.setInizationalSystem((String) ttmMap.get("inizationalSystem"));
        if (ttmMap.get("businessReference") != null)
            ttm.setBusinessReference((String) ttmMap.get("businessReference"));
        if (ttmMap.get("externalId") != null)
            ttm.setExternalId((String) ttmMap.get("externalId"));
        if (ttmMap.get("senderBic") != null)
            ttm.setSenderBic((String) ttmMap.get("senderBic"));
        if (ttmMap.get("receiverBic") != null)
            ttm.setReceiverBic((String) ttmMap.get("receiverBic"));
        if (ttmMap.get("type") != null)
            ttm.setType((String) ttmMap.get("type"));
        if (ttmMap.get("style") != null)
            ttm.setStyle((int) ttmMap.get("style"));
        if (ttmMap.get("product") != null)
            ttm.setProduct((String) ttmMap.get("product"));
        if (ttmMap.get("customerId") != null)
            ttm.setCustomerId((int) ttmMap.get("customerId"));
        if (ttmMap.get("branchId") != null)
            ttm.setBranchId((int) ttmMap.get("branchId"));
        if (ttmMap.get("branchLocation") != null)
            ttm.setBranchLocation((String) ttmMap.get("branchLocation"));

        if (ttmMap.get("relatedTtmRef") != null)
            ttm.setRelatedTtmRef((String) ttmMap.get("relatedTtmRef"));
        if (ttmMap.get("mbe") != null)
            ttm.setMbe((String) ttmMap.get("mbe"));
        if (ttmMap.get("systemId") != null)
            ttm.setSystemId((String) ttmMap.get("systemId"));
        if (ttmMap.get("tag20") != null)
            ttm.setTag20((String) ttmMap.get("tag20"));
        if (ttmMap.get("tag21") != null)
            ttm.setTag21((String) ttmMap.get("tag21"));
        if (ttmMap.get("trafinasBranchId") != null)
            ttm.setTrafinasBranchId((String) ttmMap.get("trafinasBranchId"));
        if (ttmMap.get("trafinasCaseId") != null)
            ttm.setTrafinasCaseId((String) ttmMap.get("trafinasCaseId"));
        if (ttmMap.get("trafinasEventId") != null)
            ttm.setTrafinasEventId((String) ttmMap.get("trafinasEventId"));
        if (ttmMap.get("trafinasEventId") != null)
            ttm.setTrafinasEventId((String) ttmMap.get("trafinasEventId"));
        if (ttmMap.get("otherSystem") != null)
            ttm.setOtherSystem((String) ttmMap.get("otherSystem"));
        if (ttmMap.get("createdAt") != null)
            ttm.setCreatedAt(LocalDateTime.parse((CharSequence)ttmMap.get("createdAt")));
        if (ttmMap.get("doneAt") != null)
            ttm.setDoneAt(LocalDateTime.now());
        if (ttmMap.get("trafinasSendReqAt") != null)
            ttm.setTrafinasSendReqAt(LocalDateTime.parse((CharSequence)ttmMap.get("trafinasSendReqAt")));
        if (ttmMap.get("trafinasGetResAt") != null)
            ttm.setTrafinasSendReqAt(LocalDateTime.parse((CharSequence)ttmMap.get("trafinasGetResAt")));

        return ttm;
    }

    public TTMTable getdDataByReference(final String filterRequestJSON) throws Exception {
        if (filterRequestJSON != null && !filterRequestJSON.isEmpty()) {
            Map<String, Object> jdata = new HashMap<String, Object>();
            jdata.put("ttmReference", filterRequestJSON);
            Map<String, Object> req = new HashMap<String, Object>();
            req.put("cmd", "get");
            req.put("filer", "all");
            req.put("data", jdata);
            String sendMsg = Helpers.map2json(req);
            sender.send2queue(sendMsg, "inbound.queue");
            TimeUnit.SECONDS.sleep(3);
            Map<String, Object> reply = new HashMap<String, Object>();
            reply = Helpers.json2map(receiver.latestMsgInTopic);
            return map2table( reply );
        }
        return new TTMTable();
    }

    public List<TTMTable> getFileredData(final String filterRequestJSON) throws Exception {
        List<TTMTable> table = new ArrayList<>();
        Map<String, Object> jdata =  Helpers.json2map(filterRequestJSON);
        if (jdata != null && !jdata.isEmpty()) {
            logger.info(String.format("Filtering data by: %s", filterRequestJSON));
            Map<String, Object> req = new HashMap<String, Object>();
            req.put("cmd", "filter");
            req.put("flter", "all");
            req.put("data", jdata);
            String sendMsg = Helpers.map2json(req);
            sender.send2queue(sendMsg, "inbound.queue");
            TimeUnit.SECONDS.sleep(3);
            System.out.println("\n\n testJMSManagerUpdate.Receive message=[" + receiver.latestMsgInTopic + "]\n");
            JSONTokener parser = new JSONTokener(receiver.latestMsgInTopic);
            JSONArray records = new JSONArray(parser);
            for (Object record : records) {
                JSONObject tmp = (JSONObject) record;
                Map<String, Object> reply = new HashMap<String, Object>();
                reply = Helpers.json2map(tmp.toString());
                TTMTable tableRecord = map2table(reply);
                if (tableRecord != null) table.add(tableRecord);
            }
        }
        return table;
    }

    public TTMTable updateTableData(final String filterRequestJSON) throws Exception {
        Map<String, Object> jdata =  Helpers.json2map(filterRequestJSON);
        if (jdata != null && !jdata.isEmpty()) {
            logger.info(String.format("Updating data: %s", filterRequestJSON));
            Map<String, Object> req = new HashMap<String, Object>();
            req.put("cmd", "update");
            req.put("filer", "all");
            req.put("data", jdata);
            String sendMsg = Helpers.map2json(req);
            sender.send2queue(sendMsg, "inbound.queue");
            TimeUnit.SECONDS.sleep(3);
            Map<String, Object> reply = new HashMap<String, Object>();
            reply = Helpers.json2map(receiver.latestMsgInTopic);
            return map2table( reply );
        }
        return new TTMTable();
    }
}

