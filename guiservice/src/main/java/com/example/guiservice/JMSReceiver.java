package com.example.guiservice;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class JMSReceiver implements MessageListener {
    private final Logger log = LoggerFactory.getLogger(getClass());
    String latestMsgInQueue;
    String latestMsgInTopic;

    @Autowired
    JmsTemplate jmsTemplate;

    @JmsListener(destination = "inbound.queue")
    public void onMessage(Message message) {
        String msg = null;
        try {
            msg = ((TextMessage) message).getText();
        } catch (JMSException e) {
            e.printStackTrace();
            this.latestMsgInQueue = null;
        }
        log.info("onMessage: >>> Message has been consumed : " + msg);
        System.out.println("onMessage: Message has been consumed : " + msg);
        this.latestMsgInQueue = msg;
    }

    @JmsListener(destination = "outbound.topics", selector = "filter = 'all'")
    public void onMessageTopic(Message message) throws JMSException {
        String msg = null;
        try {
            msg = ((TextMessage) message).getText();
        } catch (JMSException e) {
            e.printStackTrace();
            this.latestMsgInQueue = null;
        }
        log.info("onMessageTopic: >>> Message has been consumed : " + msg);
        System.out.println("onMessageTopic: Message has been consumed : " + msg);
        this.latestMsgInTopic = msg;
    }
}
