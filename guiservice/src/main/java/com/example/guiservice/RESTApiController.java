package com.example.guiservice;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/guiservice")
public class RESTApiController {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JMSTTMTableService ttmService;

    /**
     * get version
     *
     * @param None
     * @return json
     * @throws None
     */
    @PostMapping("/ver")
    @ResponseBody
    public Map<String, Object> getVer() {
        Map<String, Object> reply = new HashMap<String, Object>();
        reply.put("version", "1.0");
        log.debug("getVer:" + reply.toString());
        return reply;
    }

    /**
     * get records from DB by filer
     *
     * @param json filter
     * @return List
     * @throws None
     */
    @PostMapping("/filter")
    public Iterable<TTMTable> getByFilter(@RequestBody String ttmJson) {
        log.info("\n.............. filter::jsondata ............ =" + ttmJson.toString());
        try {
            return ttmService.getFileredData(ttmJson);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * get one records from DB by ttmReference number
     *
     * @param ttmJson - json string
     * @return TTMTable obj as json
     * @throws None
     */
    @PostMapping("/get")
    public TTMTable getDataByReference(@RequestBody String ttmJson) {
        log.info("\n.............. jsondata ............ =" + ttmJson.toString());
        try {
            return ttmService.getdDataByReference(ttmJson);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * update record
     *
     * @param ttmJson - json string
     * @return TTMTable obj as json otherwise TTMTable obj with Error message inside
     * @throws None
     */
    @PostMapping("/update")
    public TTMTable updateTable(@RequestBody String ttmJson) {
        log.info("\n.............. jsondata ............ =" + ttmJson.toString());
        Map<String, Object> ttmMap;
        ttmMap = Helpers.json2map(ttmJson.toString());
        log.info("\n.............. mapdata ............. =" + ttmMap.toString());
        try {
            return ttmService.updateTableData(ttmJson);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
