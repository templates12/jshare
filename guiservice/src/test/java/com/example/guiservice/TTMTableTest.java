package com.example.guiservice;


import org.junit.jupiter.api.Test;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
@SpringBootTest
@ContextConfiguration
class TTMTableTest {

    @Test
    public void testTtmtable() {
        TTMTable ttm = new TTMTable();
        ttm.setTtmReference(Helpers.generateString(12));
        ttm.setStatus("foo");

        Assert.assertNotNull(ttm.getTtmReference());
        Assert.assertNotNull(ttm.getStatus());
    }
}