package com.example.guiservice;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
@SpringBootTest
@ContextConfiguration
@AutoConfigureMockMvc
class RESTApiControllerTest {
    @Autowired
    private MockMvc mockMvc;


    @Test
    public void apiTestVer() throws Exception {
        mockMvc.perform(post("/guiservice/ver"))
                .andExpect(jsonPath("$.version", is(notNullValue())));
    }

    @Test
    public void apiTestGet() throws Exception {
        TTMTable ttm = new TTMTable();
        ttm.setTtmReference(Helpers.generateString(12));
        Map<String, Object> req = new HashMap<String, Object>();
        req.put("ttmReference", ttm.getTtmReference());
        String json = Helpers.map2json(req);
        System.out.println("JSON="+json);

        mockMvc.perform(post("/guiservice/get").contentType(MediaType.APPLICATION_JSON).content(json))
                //.andExpect(jsonPath("$.ttmReference", is(ttm.getTtmReference())))
                .andExpect(status().isOk());
    }

    @Test
    public void apiTestUpdate() throws Exception {
        TTMTable ttm = new TTMTable();
        ttm.setTtmReference(Helpers.generateString(12));
        Map<String, Object> req = new HashMap<String, Object>();
        req.put("ttmReference", ttm.getTtmReference());
        req.put("status", "foobarbaz");
        String json = Helpers.map2json(req);
        System.out.println("JSON="+json);

        mockMvc.perform(post("/guiservice/update").contentType(MediaType.APPLICATION_JSON).content(json))
                //.andExpect(jsonPath("$.ttmReference", is(ttm.getTtmReference())))
                //.andExpect(jsonPath("$.status", is("foobarbaz")))
                .andExpect(status().isOk());
    }

    @Test
    public void apiTestFilter() throws Exception {
        TTMTable ttm1 = new TTMTable();
        ttm1.setTtmReference(Helpers.generateString(12));
        TTMTable ttm2 = new TTMTable();
        ttm2.setTtmReference(Helpers.generateString(12));

        Map<String, Object> req = new HashMap<String, Object>();
        req.put("ttmReference", ttm1.getTtmReference());
        req.put("status", "foobarbaz");
        String json = Helpers.map2json(req);
        System.out.println("JSON="+json);
        mockMvc.perform(post("/guiservice/update").contentType(MediaType.APPLICATION_JSON).content(json));

        Map<String, Object> req1 = new HashMap<String, Object>();
        req1.put("ttmReference", ttm2.getTtmReference());
        req1.put("status", "NA");
        String json1 = Helpers.map2json(req1);
        System.out.println("JSON1="+json1);
        mockMvc.perform(post("/guiservice/update").contentType(MediaType.APPLICATION_JSON).content(json1));

        Map<String, Object> req2 = new HashMap<String, Object>();
        req2.put("status", "foobarbaz");
        String json2 = Helpers.map2json(req2);
        System.out.println("JSON2="+json2);

        mockMvc.perform(post("/guiservice/filter").contentType(MediaType.APPLICATION_JSON).content(json2))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
                //.andExpect(jsonPath("$[0].ttmReference", is(ttm1.getTtmReference())));
    }
}